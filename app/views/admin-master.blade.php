<!DOCTYPE html>
<html lang="en" ng-app="monitorReto">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />
    <title>Reto 21 dias - monitorde administracion</title>
    {{ HTML::style('vendor/kendo/materialize.min.css') }}
    {{ HTML::style('vendor/kendo/kendo.common.min.css') }}
    {{ HTML::style('vendor/kendo/kendo.dataviz.min.css') }}
    {{ HTML::style('vendor/kendo/kendo.material.min.css') }}
    {{ HTML::style('css/styles_monitor.css') }}
    @yield('admin-styles')

    {{ HTML::script('vendor/jquery/jquery-1.11.2.min.js') }}
    {{ HTML::script('vendor/angular/angular.min.js') }}
    {{ HTML::script('vendor/angular-route/angular-route.min.js') }}
    {{ HTML::script('vjs/app.monitor-reto.js') }}
    @yield('admin-header-scripts')
</head>

<body ng-controller="AppController">
    @include('elements.admin.topbar')

    <div ng-view></div>

    {{ HTML::script('vendor/kendo/kendo.all.min.js') }}
    {{ HTML::script('http://cdn.kendostatic.com/2014.3.1029/js/jszip.min.js') }}
    {{ HTML::script('vendor/materialize/materialize.min.js') }}
    @yield('admin-footer-scripts')
</body>
</html>