<!doctype html>
<html lang="en">
<head>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    {{ HTML::style('vendor/bootstrap/bootstrap.min.css') }}
    {{ HTML::style('vendor/slick/slick.css') }}
    {{ HTML::style('vendor/slick/slick-theme.css') }}
    {{ HTML::style('vendor/animatecss/animate.css') }}
    @yield('styles')
</head>

<body>
<div class="introduccion-reto">
    @yield('intro')
</div>

<div class="retadores">
    @yield('numero')
</div>

<div class="shadow"></div>

<div class="motivacion">
    @yield('motivations')
</div>
<div class="aprende">
    @yield('bitacora')
</div>
<div class="lugares">
    @yield('lugares')
</div>
<div class="demas-retos">
    @yield('participantes')

</div>

{{ HTML::script('vendor/jquery/jquery-1.11.2.min.js') }}
{{ HTML::script('vendor/bootstrap/bootstrap.min.js') }}
{{ HTML::script('vendor/angular/angular.min.js') }}
{{ HTML::script('vendor/angular-resource/angular-resource.min.js') }}
{{ HTML::script('vendor/angular-sanitize/angular-sanitize.min.js') }}
{{ HTML::script('vendor/slick/slick.min.js') }}
{{ HTML::script('js/app.historias.js') }}
@yield('scripts')
</body>
</html>