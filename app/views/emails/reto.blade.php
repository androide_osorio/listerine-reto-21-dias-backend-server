<!-- Inliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width" />
</head>
<body style="width: 100% !important; min-width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; margin: 0; padding: 0;"><style type="text/css">
    @font-face {
        font-family: 'Montserrat'; font-style: normal; font-weight: 400; src: local('Montserrat-Regular'), url('http://fonts.gstatic.com/s/montserrat/v6/zhcz-_WihjSQC0oHJ9TCYC3USBnSvpkopQaUR-2r7iU.ttf') format('truetype');
    }
    @font-face {
        font-family: 'Montserrat'; font-style: normal; font-weight: 700; src: local('Montserrat-Bold'), url('http://fonts.gstatic.com/s/montserrat/v6/IQHow_FEYlDC4Gzy_m8fcvEr6Hm6RMS0v1dtXsGir4g.ttf') format('truetype');
    }
    a:hover {
        color: #2795b6 !important;
    }
    a:active {
        color: #2795b6 !important;
    }
    a:visited {
        color: #2ba6cb !important;
    }
    h1 a:active {
        color: #2ba6cb !important;
    }
    h2 a:active {
        color: #2ba6cb !important;
    }
    h3 a:active {
        color: #2ba6cb !important;
    }
    h4 a:active {
        color: #2ba6cb !important;
    }
    h5 a:active {
        color: #2ba6cb !important;
    }
    h6 a:active {
        color: #2ba6cb !important;
    }
    h1 a:visited {
        color: #2ba6cb !important;
    }
    h2 a:visited {
        color: #2ba6cb !important;
    }
    h3 a:visited {
        color: #2ba6cb !important;
    }
    h4 a:visited {
        color: #2ba6cb !important;
    }
    h5 a:visited {
        color: #2ba6cb !important;
    }
    h6 a:visited {
        color: #2ba6cb !important;
    }
    table.button:hover td {
        background: #2795b6 !important;
    }
    table.button:visited td {
        background: #2795b6 !important;
    }
    table.button:active td {
        background: #2795b6 !important;
    }
    table.button:hover td a {
        color: #fff !important;
    }
    table.button:visited td a {
        color: #fff !important;
    }
    table.button:active td a {
        color: #fff !important;
    }
    table.button:hover td {
        background: #2795b6 !important;
    }
    table.tiny-button:hover td {
        background: #2795b6 !important;
    }
    table.small-button:hover td {
        background: #2795b6 !important;
    }
    table.medium-button:hover td {
        background: #2795b6 !important;
    }
    table.large-button:hover td {
        background: #2795b6 !important;
    }
    table.button:hover td a {
        color: #ffffff !important;
    }
    table.button:active td a {
        color: #ffffff !important;
    }
    table.button td a:visited {
        color: #ffffff !important;
    }
    table.tiny-button:hover td a {
        color: #ffffff !important;
    }
    table.tiny-button:active td a {
        color: #ffffff !important;
    }
    table.tiny-button td a:visited {
        color: #ffffff !important;
    }
    table.small-button:hover td a {
        color: #ffffff !important;
    }
    table.small-button:active td a {
        color: #ffffff !important;
    }
    table.small-button td a:visited {
        color: #ffffff !important;
    }
    table.medium-button:hover td a {
        color: #ffffff !important;
    }
    table.medium-button:active td a {
        color: #ffffff !important;
    }
    table.medium-button td a:visited {
        color: #ffffff !important;
    }
    table.large-button:hover td a {
        color: #ffffff !important;
    }
    table.large-button:active td a {
        color: #ffffff !important;
    }
    table.large-button td a:visited {
        color: #ffffff !important;
    }
    table.secondary:hover td {
        background: #d0d0d0 !important; color: #555;
    }
    table.secondary:hover td a {
        color: #555 !important;
    }
    table.secondary td a:visited {
        color: #555 !important;
    }
    table.secondary:active td a {
        color: #555 !important;
    }
    table.success:hover td {
        background: #457a1a !important;
    }
    table.alert:hover td {
        background: #970b0e !important;
    }
    .buttonText a:hover {
        color: whitesmoke !important;
    }
    @media only screen and (max-width: 600px) {
        table[class="body"] img {
            width: auto !important; height: auto !important;
        }
        table[class="body"] center {
            min-width: 0 !important;
        }
        table[class="body"] .container {
            width: 95% !important;
        }
        table[class="body"] .row {
            width: 100% !important; display: block !important;
        }
        table[class="body"] .wrapper {
            display: block !important; padding-right: 0 !important;
        }
        table[class="body"] .columns {
            table-layout: fixed !important; float: none !important; width: 100% !important; padding-right: 0px !important; padding-left: 0px !important;
        }
        table[class="body"] .column {
            table-layout: fixed !important; float: none !important; width: 100% !important; padding-right: 0px !important; padding-left: 0px !important;
        }
        table[class="body"] .wrapper.first .columns {
            display: table !important;
        }
        table[class="body"] .wrapper.first .column {
            display: table !important;
        }
        table[class="body"] table.columns td {
            width: 100% !important;
        }
        table[class="body"] table.column td {
            width: 100% !important;
        }
        table[class="body"] .columns td.one {
            width: 8.333333% !important;
        }
        table[class="body"] .column td.one {
            width: 8.333333% !important;
        }
        table[class="body"] .columns td.two {
            width: 16.666666% !important;
        }
        table[class="body"] .column td.two {
            width: 16.666666% !important;
        }
        table[class="body"] .columns td.three {
            width: 25% !important;
        }
        table[class="body"] .column td.three {
            width: 25% !important;
        }
        table[class="body"] .columns td.four {
            width: 33.333333% !important;
        }
        table[class="body"] .column td.four {
            width: 33.333333% !important;
        }
        table[class="body"] .columns td.five {
            width: 41.666666% !important;
        }
        table[class="body"] .column td.five {
            width: 41.666666% !important;
        }
        table[class="body"] .columns td.six {
            width: 50% !important;
        }
        table[class="body"] .column td.six {
            width: 50% !important;
        }
        table[class="body"] .columns td.seven {
            width: 58.333333% !important;
        }
        table[class="body"] .column td.seven {
            width: 58.333333% !important;
        }
        table[class="body"] .columns td.eight {
            width: 66.666666% !important;
        }
        table[class="body"] .column td.eight {
            width: 66.666666% !important;
        }
        table[class="body"] .columns td.nine {
            width: 75% !important;
        }
        table[class="body"] .column td.nine {
            width: 75% !important;
        }
        table[class="body"] .columns td.ten {
            width: 83.333333% !important;
        }
        table[class="body"] .column td.ten {
            width: 83.333333% !important;
        }
        table[class="body"] .columns td.eleven {
            width: 91.666666% !important;
        }
        table[class="body"] .column td.eleven {
            width: 91.666666% !important;
        }
        table[class="body"] .columns td.twelve {
            width: 100% !important;
        }
        table[class="body"] .column td.twelve {
            width: 100% !important;
        }
        table[class="body"] td.offset-by-one {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-two {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-three {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-four {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-five {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-six {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-seven {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-eight {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-nine {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-ten {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-eleven {
            padding-left: 0 !important;
        }
        table[class="body"] table.columns td.expander {
            width: 1px !important;
        }
        table[class="body"] .right-text-pad {
            padding-left: 10px !important;
        }
        table[class="body"] .text-pad-right {
            padding-left: 10px !important;
        }
        table[class="body"] .left-text-pad {
            padding-right: 10px !important;
        }
        table[class="body"] .text-pad-left {
            padding-right: 10px !important;
        }
        table[class="body"] .hide-for-small {
            display: none !important;
        }
        table[class="body"] .show-for-desktop {
            display: none !important;
        }
        table[class="body"] .show-for-small {
            display: inherit !important;
        }
        table[class="body"] .hide-for-desktop {
            display: inherit !important;
        }
        .textRetadores {
            height: auto; margin: 25px auto;
        }
        h4 {
            text-align: center;
        }
        p {
            text-align: center;
        }
        .infoParticipante {
            background: #ffffff; border-bottom: solid 2px whitesmoke; border-top: solid 2px whitesmoke;
        }
    }
</style>
<table class="body" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="center" align="center" valign="top" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;">
            <center style="width: 100%; min-width: 580px;">

                <table class="container" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; width: 580px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">
                            <!-- content -->
                            <p style="color: #fff; display: none; font-family: 'Montserrat', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 22px; margin: 0 0 10px; padding: 0;" align="left">(PREHEADER)</p>
                            <!-- ROW CHORRO -->
                            <table class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0px;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">

                                        <!-- COLUMNS -->
                                        <table class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; background: #ffffff url(http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/chorro.jpg) top left; margin: 0 auto; padding: 0;" bgcolor="#ffffff" background="url(http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/chorro.jpg)"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 0px 10px;" align="left" valign="top">
                                                    <img src="http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/logo_listerine.png" alt="" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: left; clear: both; display: block; padding: 15px;" align="left" /></td>
                                            </tr></table></td>
                                </tr></table><!-- ROW Title reto --><table class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0px;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">

                                        <!-- COLUMNS -->
                                        <table class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="nine sub-columns" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; min-width: 0px; width: 75%; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 25px 0px 25px 25px;" align="left" valign="top">
                                                    <img class="center" alt="PRESENTA 4 RETADORES EN SU TRANSFORMACIÓN REAL" src="http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/texto4retadores.png" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" align="none" /></td>
                                                <td class="three sub-columns" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; min-width: 0px; width: 25%; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 15px;" align="left" valign="top">
                                                    <img class=" center" src="http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/21-dias.png" alt="" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" align="none" /></td>
                                            </tr></table></td>
                                </tr></table><!-- ROW sombra --><table class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; background: rgb(238,249,247); padding: 0px;" bgcolor="rgb(238,249,247)"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">
                                        <!-- COLUMNS -->
                                        <table class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px;" align="left" valign="top">
                                                    <img class="resposive center" src="http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/sombra.png" alt="" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" align="none" /></td>
                                            </tr></table><table class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0px;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">
                                                    <!-- COLUMNS -->
                                                    <table class="four-up columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; width: 125px; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 10px;" align="left" valign="top">
                                                                <img src="http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/beto.png" class="center" alt="" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" align="none" /><p class="textRetadores" style="color: rgb(38,152,158); font-family: 'Montserrat', sans-serif; font-weight: normal; text-align: center; line-height: 19px; font-size: 12px; font-style: bold !important; height: 130px; max-width: 200px; margin: auto; padding: 0;" align="center">“Siempre he
                                                                    querido tocar mi canción favorita en guitarra, esta es
                                                                    mi oportunidad”</p>
                                                                <img src="http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/guitarra.png" class="center hide-for-small" alt="" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" align="none" /></td>

                                                        </tr></table></td>
                                                <td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">
                                                    <!-- COLUMNS -->
                                                    <table class="four-up columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; width: 125px; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 10px;" align="left" valign="top">
                                                                <img src="http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/virginia.png" class="center" alt="" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" align="none" /><p class="textRetadores" style="color: rgb(38,152,158); font-family: 'Montserrat', sans-serif; font-weight: normal; text-align: center; line-height: 19px; font-size: 12px; font-style: bold !important; height: 130px; max-width: 200px; margin: auto; padding: 0;" align="center">“Todos en mi familia son amantes de la salsa, MI RETO es aprender a bailar salsa”</p>
                                                                <img src="http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/charol.png" class="center hide-for-small" alt="" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" align="none" /></td>

                                                        </tr></table></td>
                                                <td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">
                                                    <!-- COLUMNS -->
                                                    <table class="four-up columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; width: 125px; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 10px;" align="left" valign="top">
                                                                <img src="http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/cesar.png" class="center" alt="" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" align="none" /><p class="textRetadores" style="color: rgb(38,152,158); font-family: 'Montserrat', sans-serif; font-weight: normal; text-align: center; line-height: 19px; font-size: 12px; font-style: bold !important; height: 130px; max-width: 200px; margin: auto; padding: 0;" align="center">“Sentirme y verme mejor, cambiar mis habitos de comida y adoptar un estilo de vida saluble es MI RETO ”</p>
                                                                <img src="http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/bascula.png" class="center hide-for-small" alt="" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" align="none" /></td>

                                                        </tr></table></td><td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">
                                                    <!-- COLUMNS -->
                                                    <table class="four-up columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; width: 125px; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 10px;" align="left" valign="top">
                                                                <img src="http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/jose.png" class="center" alt="" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" align="none" /><p class="textRetadores" style="color: rgb(38,152,158); font-family: 'Montserrat', sans-serif; font-weight: normal; text-align: center; line-height: 19px; font-size: 12px; font-style: bold !important; height: 130px; max-width: 200px; margin: auto; padding: 0;" align="center">“El estrés diario y la rutina en mi trabajo
                                                                    quedarán atrás con MIS CLASES DE YOGA ”</p>
                                                                <img src="http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/yoga.png" class="center hide-for-small" alt="" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" align="none" /></td>

                                                        </tr></table></td>

                                            </tr></table></td>
                                </tr></table><!-- ROW button --><table class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0px;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">

                                        <!-- COLUMNS -->
                                        <table class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 0px 10px;" align="left" valign="top">
                                                    <center style="width: 100%; min-width: 580px;">
                                                        <br /><br /><table class="radius" style="width: 85%; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="buttonText" style="border-bottom-style: solid; border-bottom-color: rgb(15,114,119); border-bottom-width: 5px; word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #222222; font-family: 'Montserrat', sans-serif; font-weight: normal; line-height: 19px; font-size: 20px; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; background: rgb(38,152,158); margin: 0; padding: 15px 5px;" align="center" bgcolor="rgb(38,152,158)" valign="top">

                                                                    <a href="#" style="color: white; text-decoration: none;"><italic>SIGUE LAS HISTORIAS Y </italic><strong>ÚNETE A SU RETO</strong></a>

                                                                </td>
                                                            </tr></table><br /><br /></center></td>

                                            </tr></table></td>
                                </tr></table><!-- ROW footer --><table class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; background: #d1d1d1; padding: 20px 0px 0px;" bgcolor="#d1d1d1"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">

                                        <!-- COLUMNS -->
                                        <table class="eleven columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 530px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="vertical-align: middle; color: #FFF; word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; text-align: left; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 0px 10px;" align="left" valign="middle">
                                                    <div style="box-sizing: border-box; padding: 0px 15px;">
                                                        <br /><h4 class="titleFoot" style="color: rgb(72,72,72); font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 1.3; word-break: normal; font-size: 12px; font-style: bold; margin: 0; padding: 0;" align="left">© Johnson &amp; Johnson de Colombia S.A. 2015</h4>
                                                        <p class="footText" style="color: rgb(72,72,72); font-family: 'Montserrat', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 10px; margin: 0 0 10px; padding: 0;" align="left">
                                                            Este e-mail se envía de acuerdo a la información de tu registro en www.jnjcolombia.com. Copyright de todo el contenido y de los artículos en www.jnjcolombia.com.
                                                            <br /><br /> Los sitios de internet de Johnson & Johnson proveen información general, con propósito educativo solamente. Cualquier preocupación o problema de salud, tuyo o de tu familia, requiere una consulta a un doctor o a otro profesional de la salud. Por favor revisa la Política de privacidad y la Nota legal antes de usar este sitio. Al usar el sitio indicas que estás de acuerdo y aceptas dichas políticas.
                                                        </p>
                                                    </div>
                                                </td>
                                                <td class="expander" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top"></td>
                                            </tr></table></td>

                                    <td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">

                                        <!-- COLUMNS -->
                                        <table class="one columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 30px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="vertical-align: top; color: #FFF; word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; text-align: left; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 0px 10px;" align="left" valign="top">
                                                    <center style="width: 100%; min-width: 30px;">
                                                        <a href="#" target="_blank" style="color: #2ba6cb; text-decoration: none;">
                                                            <img src="http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/icono_facebook.png" class="center" style="display: inline-block; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; margin: 0 auto 10px; border: none;" align="none" /></a>

                                                        <a href="#" target="_blank" style="color: #2ba6cb; text-decoration: none;">
                                                            <img src="http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/icono_twitter.png" class="center" style="display: inline-block; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; margin: 0 auto 10px; border: none;" align="none" /></a>

                                                        <a href="https://www.youtube.com/user/ListerineCO" style="color: #2ba6cb; text-decoration: none;">
                                                            <img src="http://owak.co/emailMarketing_listerine/img_EM-seguimiento2/icono_youtube.png" class="center" style="display: inline-block; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; margin: 0 auto 10px; border: none;" align="none" /></a>
                                                    </center>
                                                </td>
                                                <td class="expander" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top"></td>
                                            </tr></table></td>



                                </tr></table><!-- cierra content --></td>
                    </tr></table></center>
        </td>
    </tr></table></body>
</html>