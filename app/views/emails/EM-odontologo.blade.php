<!-- Inliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width" />
</head>
<body style="width: 100% !important; min-width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; margin: 0; padding: 0;"><style type="text/css">
    @font-face {
        font-family: 'Montserrat'; font-style: normal; font-weight: 400; src: local('Montserrat-Regular'), url('http://fonts.gstatic.com/s/montserrat/v6/zhcz-_WihjSQC0oHJ9TCYC3USBnSvpkopQaUR-2r7iU.ttf') format('truetype');
    }
    @font-face {
        font-family: 'Montserrat'; font-style: normal; font-weight: 700; src: local('Montserrat-Bold'), url('http://fonts.gstatic.com/s/montserrat/v6/IQHow_FEYlDC4Gzy_m8fcvEr6Hm6RMS0v1dtXsGir4g.ttf') format('truetype');
    }
    a:hover {
        color: #2795b6 !important;
    }
    a:active {
        color: #2795b6 !important;
    }
    a:visited {
        color: #2ba6cb !important;
    }
    h1 a:active {
        color: #2ba6cb !important;
    }
    h2 a:active {
        color: #2ba6cb !important;
    }
    h3 a:active {
        color: #2ba6cb !important;
    }
    h4 a:active {
        color: #2ba6cb !important;
    }
    h5 a:active {
        color: #2ba6cb !important;
    }
    h6 a:active {
        color: #2ba6cb !important;
    }
    h1 a:visited {
        color: #2ba6cb !important;
    }
    h2 a:visited {
        color: #2ba6cb !important;
    }
    h3 a:visited {
        color: #2ba6cb !important;
    }
    h4 a:visited {
        color: #2ba6cb !important;
    }
    h5 a:visited {
        color: #2ba6cb !important;
    }
    h6 a:visited {
        color: #2ba6cb !important;
    }
    table.button:hover td {
        background: #2795b6 !important;
    }
    table.button:visited td {
        background: #2795b6 !important;
    }
    table.button:active td {
        background: #2795b6 !important;
    }
    table.button:hover td a {
        color: #fff !important;
    }
    table.button:visited td a {
        color: #fff !important;
    }
    table.button:active td a {
        color: #fff !important;
    }
    table.button:hover td {
        background: #2795b6 !important;
    }
    table.tiny-button:hover td {
        background: #2795b6 !important;
    }
    table.small-button:hover td {
        background: #2795b6 !important;
    }
    table.medium-button:hover td {
        background: #2795b6 !important;
    }
    table.large-button:hover td {
        background: #2795b6 !important;
    }
    table.button:hover td a {
        color: #ffffff !important;
    }
    table.button:active td a {
        color: #ffffff !important;
    }
    table.button td a:visited {
        color: #ffffff !important;
    }
    table.tiny-button:hover td a {
        color: #ffffff !important;
    }
    table.tiny-button:active td a {
        color: #ffffff !important;
    }
    table.tiny-button td a:visited {
        color: #ffffff !important;
    }
    table.small-button:hover td a {
        color: #ffffff !important;
    }
    table.small-button:active td a {
        color: #ffffff !important;
    }
    table.small-button td a:visited {
        color: #ffffff !important;
    }
    table.medium-button:hover td a {
        color: #ffffff !important;
    }
    table.medium-button:active td a {
        color: #ffffff !important;
    }
    table.medium-button td a:visited {
        color: #ffffff !important;
    }
    table.large-button:hover td a {
        color: #ffffff !important;
    }
    table.large-button:active td a {
        color: #ffffff !important;
    }
    table.large-button td a:visited {
        color: #ffffff !important;
    }
    table.secondary:hover td {
        background: #d0d0d0 !important; color: #555;
    }
    table.secondary:hover td a {
        color: #555 !important;
    }
    table.secondary td a:visited {
        color: #555 !important;
    }
    table.secondary:active td a {
        color: #555 !important;
    }
    table.success:hover td {
        background: #457a1a !important;
    }
    table.alert:hover td {
        background: #970b0e !important;
    }
    @media only screen and (max-width: 600px) {
        table[class="body"] img {
            width: auto !important; height: auto !important;
        }
        table[class="body"] center {
            min-width: 0 !important;
        }
        table[class="body"] .container {
            width: 95% !important;
        }
        table[class="body"] .row {
            width: 100% !important; display: block !important;
        }
        table[class="body"] .wrapper {
            display: block !important; padding-right: 0 !important;
        }
        table[class="body"] .columns {
            table-layout: fixed !important; float: none !important; width: 100% !important; padding-right: 0px !important; padding-left: 0px !important;
        }
        table[class="body"] .column {
            table-layout: fixed !important; float: none !important; width: 100% !important; padding-right: 0px !important; padding-left: 0px !important;
        }
        table[class="body"] .wrapper.first .columns {
            display: table !important;
        }
        table[class="body"] .wrapper.first .column {
            display: table !important;
        }
        table[class="body"] table.columns td {
            width: 100% !important;
        }
        table[class="body"] table.column td {
            width: 100% !important;
        }
        table[class="body"] .columns td.one {
            width: 8.333333% !important;
        }
        table[class="body"] .column td.one {
            width: 8.333333% !important;
        }
        table[class="body"] .columns td.two {
            width: 16.666666% !important;
        }
        table[class="body"] .column td.two {
            width: 16.666666% !important;
        }
        table[class="body"] .columns td.three {
            width: 25% !important;
        }
        table[class="body"] .column td.three {
            width: 25% !important;
        }
        table[class="body"] .columns td.four {
            width: 33.333333% !important;
        }
        table[class="body"] .column td.four {
            width: 33.333333% !important;
        }
        table[class="body"] .columns td.five {
            width: 41.666666% !important;
        }
        table[class="body"] .column td.five {
            width: 41.666666% !important;
        }
        table[class="body"] .columns td.six {
            width: 50% !important;
        }
        table[class="body"] .column td.six {
            width: 50% !important;
        }
        table[class="body"] .columns td.seven {
            width: 58.333333% !important;
        }
        table[class="body"] .column td.seven {
            width: 58.333333% !important;
        }
        table[class="body"] .columns td.eight {
            width: 66.666666% !important;
        }
        table[class="body"] .column td.eight {
            width: 66.666666% !important;
        }
        table[class="body"] .columns td.nine {
            width: 75% !important;
        }
        table[class="body"] .column td.nine {
            width: 75% !important;
        }
        table[class="body"] .columns td.ten {
            width: 83.333333% !important;
        }
        table[class="body"] .column td.ten {
            width: 83.333333% !important;
        }
        table[class="body"] .columns td.eleven {
            width: 91.666666% !important;
        }
        table[class="body"] .column td.eleven {
            width: 91.666666% !important;
        }
        table[class="body"] .columns td.twelve {
            width: 100% !important;
        }
        table[class="body"] .column td.twelve {
            width: 100% !important;
        }
        table[class="body"] td.offset-by-one {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-two {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-three {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-four {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-five {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-six {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-seven {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-eight {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-nine {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-ten {
            padding-left: 0 !important;
        }
        table[class="body"] td.offset-by-eleven {
            padding-left: 0 !important;
        }
        table[class="body"] table.columns td.expander {
            width: 1px !important;
        }
        table[class="body"] .right-text-pad {
            padding-left: 10px !important;
        }
        table[class="body"] .text-pad-right {
            padding-left: 10px !important;
        }
        table[class="body"] .left-text-pad {
            padding-right: 10px !important;
        }
        table[class="body"] .text-pad-left {
            padding-right: 10px !important;
        }
        table[class="body"] .hide-for-small {
            display: none !important;
        }
        table[class="body"] .show-for-desktop {
            display: none !important;
        }
        table[class="body"] .show-for-small {
            display: inherit !important;
        }
        table[class="body"] .hide-for-desktop {
            display: inherit !important;
        }
        h4 {
            text-align: center;
        }
        p {
            text-align: center;
        }
        .infoParticipante {
            background: #ffffff; border-bottom: solid 2px whitesmoke; border-top: solid 2px whitesmoke;
        }
    }
</style>
<table class="body" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="center" align="center" valign="top" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;">
            <center style="width: 100%; min-width: 580px;">

                <table class="container" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; width: 580px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">
                            <!-- content -->
                            <p style="color: #fff; display: none; font-family: 'helvetica', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 18px; word-wrap: normal; margin: 0 0 10px; padding: 0;" align="left">Gracias por recomendarle el reo 21 dias a tu paciente</p>
                            <br /><!-- ROW HEADER --><table class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0px;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">

                                        <!-- COLUMNS -->
                                        <table class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; background: #ffffff; margin: 0 auto; padding: 0;" bgcolor="#ffffff"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="three sub-columns" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; min-width: 0px; width: 25%; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 10px 0px 0px;" align="left" valign="top">

                                                    <img class="center " src="http://www.listerine.co/reto21dias2015/Email-marketing/logo_21dias.png" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" align="none" /><br /></td>
                                                <td class="nine sub-columns" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; min-width: 0px; width: 75%; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 10px 0px 0px;" align="left" valign="top">

                                                    <img class="center" src="http://www.listerine.co/reto21dias2015/Email-marketing/splash.png" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" align="none" /><br /><p class="center" style="font-size: 14pt; color: #008f99; text-align: center; font-family: 'helvetica', sans-serif; font-weight: normal; line-height: 19px; word-wrap: normal; margin: 0 0 10px; padding: 0;" align="center"><strong>INVITÓ A UN NUEVO PACIENTE A PARTICIPAR EN EL RETO LISTERINE® 21 DÍAS</strong></p>


                                                    <br /></td>
                                                <td class="expander" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top"></td>
                                            </tr></table></td>
                                </tr></table><!-- ROW info participante --><table class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; background: rgb(249,253,251); padding: 0px;" bgcolor="rgb(249,253,251)"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">

                                        <!-- COLUMNS -->
                                        <table class="five columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 230px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px;" align="left" valign="top">
                                                    <center style="width: 100%; min-width: 230px;">
                                                        <img style="width: 90%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" class="center" src="http://www.listerine.co/reto21dias2015/Email-marketing/listerine.png" alt="" align="none" /></center>
                                                </td>
                                            </tr></table></td>
                                    <td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">

                                        <!-- COLUMNS -->
                                        <table class="seven columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 330px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="infoParticipante" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 50px 0px 10px;" align="left" valign="top">
                                                    <h4 style="color: #222222; font-family: 'Montserrat', sans-serif; font-weight: normal; text-align: left; line-height: 1.3; word-break: normal; font-size: 20px; margin: 0; padding: 0;" align="left">El paciente</h4>
                                                    <p style="color: #222222; font-family: 'helvetica', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 18px; word-wrap: normal; margin: 0 0 10px; padding: 0;" align="left">{{ $consumer_name }}</p>
                                                    <h4 style="color: #222222; font-family: 'Montserrat', sans-serif; font-weight: normal; text-align: left; line-height: 1.3; word-break: normal; font-size: 20px; margin: 0; padding: 0;" align="left">Correo electrónico</h4>
                                                    <p style="color: #222222; font-family: 'helvetica', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 18px; word-wrap: normal; margin: 0 0 10px; padding: 0;" align="left">{{ $consumer_email }}</p>
                                                </td>
                                            </tr></table></td>

                                </tr></table><!-- ROW info participante --><table class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0px;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">

                                        <!-- COLUMNS -->
                                        <table class="four columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 180px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px;" align="left" valign="top">
                                                    <br /><center style="width: 100%; min-width: 180px;">
                                                        <img class="center" src="http://www.listerine.co/reto21dias2015/Email-marketing/reduce.png" alt="" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" align="none" /></center>
                                                </td>
                                            </tr></table></td>
                                    <td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">

                                        <!-- COLUMNS -->
                                        <table class="eight columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 380px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 0px 10px;" align="left" valign="top">
                                                    <br /><h4 style="font-size: 15px; color: #222222; font-family: 'Montserrat', sans-serif; font-weight: normal; text-align: left; line-height: 1.3; word-break: normal; margin: 0; padding: 0;" align="left">RECUERDA USAR LISTERINE<sup>®</sup></h4>
                                                    <br /><p style="font-size: 15px; color: #222222; font-family: 'helvetica', sans-serif; font-weight: normal; text-align: left; line-height: 19px; word-wrap: normal; margin: 0 0 10px; padding: 0;" align="left">El uso de LISTERINE® antes del procedimiento,
                                                        reduce el 94% de bacterias viables en comparación
                                                        con un enjuague de control.*</p>
                                                </td>
                                            </tr></table></td>
                                </tr></table><!-- ROW mas info --><table class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0px;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">
                                        <!-- COLUMNS -->
                                        <table class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 0px 10px;" align="left" valign="top">
                                                    <center style="width: 100%; min-width: 580px;">
                                                        <p style="font-size: 8px; color: rgb(38,152,158); line-height: 10px; max-width: 470px; font-family: 'helvetica', sans-serif; font-weight: normal; text-align: left; word-wrap: normal; margin: 0 0 10px; padding: 0;" align="left">* Fine DH, Mendieta C, Barnett ML, Furgang D, Meyers R, Olshan A, Vincent J.Efficacy of preprocedural rinsing with an antiseptic in reducing viable bacteria in dental aerosols. J Periodontol. 1992 Oct;63(10):821-4.</p>
                                                    </center>
                                                </td>
                                            </tr></table></td>
                                </tr></table><!-- ROW uso listerine --><table class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; background: rgb(13,55,76); padding: 0px;" bgcolor="rgb(13,55,76)"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">

                                        <!-- COLUMNS -->
                                        <table class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 15px;" align="left" valign="top">
                                                    <h4 style="color: white; text-align: center; font-size: 15px; font-family: 'Montserrat', sans-serif; font-weight: normal; line-height: 1.3; word-break: normal; margin: 0; padding: 0;" align="center">RECUERDA RECOMENDAR EL USO DE LISTERINE<sup>®</sup> ASÍ:</h4>
                                                </td>
                                            </tr></table></td>
                                </tr></table><!-- ROW uso listerine --><table class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; background: rgb(13,55,76); padding: 0px;" bgcolor="rgb(13,55,76)"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">

                                        <!-- COLUMNS -->
                                        <table class="six columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 280px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px;" align="left" valign="top">
                                                    <center style="width: 100%; min-width: 280px;">
                                                        <img class="center" src="http://www.listerine.co/reto21dias2015/Email-marketing/pasos_botella.png" alt="uso diario 20ML durante 30 segundos" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" align="none" /></center>
                                                </td>
                                            </tr></table></td>
                                    <td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">

                                        <!-- COLUMNS -->
                                        <table class="six columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 280px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 0px 10px;" align="left" valign="top">
                                                    <center style="width: 100%; min-width: 280px;">
                                                        <img class="center" src="http://www.listerine.co/reto21dias2015/Email-marketing/pasos_tapa.png" alt="" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; display: block; margin: 0 auto;" align="none" /></center>
                                                </td>

                                            </tr></table></td>
                                </tr></table><!-- ROW footer --><table class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; background: #d1d1d1; padding: 20px 0px 0px;" bgcolor="#d1d1d1"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">

                                        <!-- COLUMNS -->
                                        <table class="eleven columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 530px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="vertical-align: middle; color: #FFF; word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; text-align: left; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 0px 10px;" align="left" valign="middle">
                                                    <div style="box-sizing: border-box; word-wrap: normal; padding: 0px 15px;">
                                                        <br /><h4 class="titleFoot" style="color: rgb(72,72,72); font-family: 'Montserrat', sans-serif; font-weight: normal; text-align: left; line-height: 1.3; word-break: normal; font-size: 12px; font-style: bold; margin: 0; padding: 0;" align="left">© Johnson &amp; Johnson de Colombia S.A. 2014</h4>
                                                        <p class="footText" style="color: rgb(72,72,72); font-family: 'helvetica', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 10px; word-wrap: normal; margin: 0 0 10px; padding: 0;" align="left">
                                                            Este e-mail se envía de acuerdo a la información de tu registro en www.jnjcolombia.com.  Copyright de todo el contenido y de los artículos en www.jnjcolombia.com. Todos los derechos reservados.
                                                            <br /><br />
                                                            Los sitios de internet de Johnson &amp; Johnson proveen información general, con propósito educativo solamente. Cualquier preocupación o problema de salud, tuyo o de tu familia, requiere una consulta a un doctor o a otro profesional de la salud. Por favor revisa la Política de privacidad y la Nota legal antes de usar este sitio. Al usar el sitio indicas que estás de acuerdo y aceptas dichas políticas.
                                                        </p>
                                                    </div>
                                                </td>
                                                <td class="expander" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top"></td>
                                            </tr></table></td>

                                    <td class="wrapper" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">

                                        <!-- COLUMNS -->
                                        <table class="one columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 30px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="vertical-align: top; color: #FFF; word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; text-align: left; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 0px 10px;" align="left" valign="top">
                                                    <center style="width: 100%; min-width: 30px;">
                                                        <a href="https://www.facebook.com/ListerineCO/" target="_blank" style="color: #2ba6cb; text-decoration: none;">
                                                            <img src="http://www.listerine.co/reto21dias2015/Email-marketing/icono_facebook.png" class="center" style="display: inline-block; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; margin: 0 auto 10px; border: none;" align="none" /></a>

                                                        <a href="https://twitter.com/listerineco" target="_blank" style="color: #2ba6cb; text-decoration: none;">
                                                            <img src="http://www.listerine.co/reto21dias2015/Email-marketing/icono_twitter.png" class="center" style="display: inline-block; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; margin: 0 auto 10px; border: none;" align="none" /></a>

                                                        <a href="https://www.youtube.com/user/ListerineCO/" style="color: #2ba6cb; text-decoration: none;">
                                                            <img src="http://www.listerine.co/reto21dias2015/Email-marketing/icono_youtube.png" class="center" style="display: inline-block; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; float: none; clear: both; margin: 0 auto 10px; border: none;" align="none" /></a>
                                                    </center>
                                                </td>
                                                <td class="expander" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top"></td>
                                            </tr></table></td>



                                </tr></table><!-- cierra content --></td>
                    </tr></table></center>
        </td>
    </tr></table></body>
</html>
