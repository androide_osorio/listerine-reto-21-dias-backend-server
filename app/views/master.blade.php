<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>formulario</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    {{ HTML::style('css/style.css') }}
    @yield('styles')
</head>
<body>
<div class="container">
    @yield('content')
</div>

{{ HTML::script('vendor/jquery/jquery-1.11.2.min.js') }}
{{ HTML::script('vendor/bootstrap-validator/validator.js') }}
@yield('scripts')
</body>
</html>
