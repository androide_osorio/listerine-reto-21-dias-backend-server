@extends('story')


@section ('styles')
{{ HTML::style('css/cesar_styles.css') }}
@stop

@section ('scripts')
{{ HTML::script('js/app_cesar.js') }}
@stop


@section('intro')
@include('elements.content.perfil', array('reto' => 'ADQUIRIR UN ESTILO DE VIDA', 'reto_descripcion' => '“MÁS SALUDABLE”', 'youtube' =>'https://www.youtube.com/user/ListerineCO/', 'facebook' =>'https://www.facebook.com/ListerineCO/', 'instagram' =>'http://instagram.com/listerineco', 'twitter' =>'https://twitter.com/listerineco', 'fotoPerfil' => 'img/cesar-perfil.png', 'tituloHabito' => 'ADQUIRIR UN ESTILO DE VIDA SALUDABLE'))
@stop


@section('numero')
@include('elements.content.retadores', array('siguelo' => 'SÍGUELO', 'contador' => $retadoresCount, 'actividad' => 'QUE ESTÁN CAMBIANDO SU ESTILO DE VIDA', 'descripcion_actividad' => 'POR UNO SALUDABLE', 'followLink' => 'https://listerinesalud.com/reto21dias2015/public/index.php/users/follow/cesar', 'nombreRetador' => 'Cesar'))
@stop


@section('motivations')
@include('elements.content.motivacion', array('listerine_reto' => 'LO APOYÓ CON:', 'motivacion' => 'Porque me gustaría sentirme mejor conmigo mismo.', 'clases' => ' ¡IMPLEMENTOS DEPORTIVOS Y PRESUPUESTO PARA COMER SALUDABLE DURANTE 21 DÍAS!', 'urlVideo' => 'https://www.youtube.com/embed/Y7V32EU2744?rel=0&showinfo=0'))
@stop



@section('bitacora')
@include('elements.content.aprender', array('aprende_tambien' => 'MEJORA TU ESTILO DE VIDA POR UNO MÁS SALUDABLE', 'retarAmigo' => 'Reta a un amigo a mejorar su estilo de vida por uno más saludable en 21 Días'))
@stop

@section('lugares')
@include('elements.content.lugares', array('imagenLugar' => 'img/lugares-guitarra.png'))
@stop

@section('participantes')
@include('elements.content.participantes', array('participanteUnoFoto' => 'vir-img', 'participanteUnoCopy' => '“Todos en mi familia son amantes de la salsa, MI RETO es aprender a bailar salsa”', 'participanteUnoLink' => 'http://www.listerine.co/reto-21-dias/aprende-a-bailar-salsa', 'participanteDosFoto' => 'beto-img', 'participanteDosCopy' => '“Siempre he querido tocar mi canción favorita en guitarra, esta es mi oportunidad”', 'participanteDosLink' => 'http://www.listerine.co/reto-21-dias/aprende-a-tocar-guitarra', 'participanteTercerFoto' => 'jose-img', 'participanteTercerCopy' => '“El estrés diario y la rutina en mi trabajo quedarán atrás con MIS CLASES DE YOGA”', 'participanteTercerLink' => 'http://www.listerine.co/reto-21-dias/disminuye-el-estres'  ))
@stop

