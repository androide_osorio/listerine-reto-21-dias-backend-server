@extends('story')


@section ('styles')
{{ HTML::style('css/virginia_styles.css') }}
@stop

@section ('scripts')
{{ HTML::script('js/app_vir.js') }}
@stop


@section('intro')
@include('elements.content.perfil', array('reto' => 'BAILAR SALSA', 'reto_descripcion' => 'COMO UNA PROFESIONAL', 'youtube' =>'https://www.youtube.com/user/ListerineCO/', 'facebook' =>'https://www.facebook.com/ListerineCO/', 'instagram' =>'http://instagram.com/listerineco', 'twitter' =>'https://twitter.com/listerineco', 'fotoPerfil' => 'img/virginia-perfil.png', 'tituloHabito' => 'APRENDE A BAILAR SALSA'))
@stop


@section('numero')
@include('elements.content.retadores', array('siguelo' => 'SÍGUELA','contador' => $retadoresCount, 'actividad' => 'QUE ESTÁN APRENDIENDO', 'descripcion_actividad' => 'A BAILAR SALSA', 'followLink' =>'https://www.listerinesalud.com/reto21dias2015/public/index.php/users/follow/virginia', 'nombreRetador' => 'Virginia'))
@stop


@section('motivations')
@include('elements.content.motivacion', array('listerine_reto' => 'LA APOYÓ CON:', 'motivacion' => 'Porque me gustaría poder salir a bailar con mis amigos y mi novio. Y ¿por qué no? Soñar con un mundial de salsa.', 'clases' => ' ¡21 CLASES DE SALSA!', 'urlVideo' => 'https://www.youtube.com/embed/tqe-xjt_Hi4?rel=0&showinfo=0'))
@stop



@section('bitacora')
@include('elements.content.aprender', array('aprende_tambien' => 'APRENDE A BAILAR SALSA TÚ TAMBIÉN', 'retarAmigo' => 'Reta a un amigo a aprender a bailar salsa en 21 Días'))
@stop

@section('lugares')
@include('elements.content.lugares', array('imagenLugar' => 'img/recomendar-salsa.png'))
@stop

@section('participantes')
@include('elements.content.participantes', array('participanteUnoFoto' => 'beto-img', 'participanteUnoCopy' => '“Siempre he querido tocar mi canción favorita en guitarra, esta es mi oportunidad”', 'participanteUnoLink' => 'http://www.listerine.co/reto-21-dias/aprende-a-tocar-guitarra', 'participanteDosFoto' => 'cesar-img', 'participanteDosCopy' => '“Sentirme y verme mejor, cambiar mis hábitos de comida y adoptar un estilo de vida saluble es MI RETO”', 'participanteDosLink' => 'http://www.listerine.co/reto-21-dias/una-vida-mas-saludable-en-21-dias', 'participanteTercerFoto' => 'jose-img', 'participanteTercerCopy' => '“El estrés diario y la rutina en mi trabajo quedarán atrás con MIS CLASES DE YOGA”', 'participanteTercerLink' => 'http://www.listerine.co/reto-21-dias/disminuye-el-estres'  ))
@stop

