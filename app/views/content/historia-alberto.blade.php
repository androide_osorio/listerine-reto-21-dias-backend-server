@extends('story')


@section ('styles')
{{ HTML::style('css/beto_styles.css') }}
@stop

@section ('scripts')
{{ HTML::script('js/app_beto.js') }}
@stop


@section('intro')
@include('elements.content.perfil', array('reto' => 'TOCAR GUITARRA', 'reto_descripcion' => '“HOTEL CALIFORNIA”', 'youtube' =>'https://www.youtube.com/user/ListerineCO/', 'facebook' =>'https://www.facebook.com/ListerineCO/', 'instagram' =>'http://instagram.com/listerineco', 'twitter' =>'https://twitter.com/listerineco', 'fotoPerfil' => 'img/beto-perfil.png', 'tituloHabito' => 'APRENDE A TOCAR GUITARRA'))
@stop


@section('numero')
@include('elements.content.retadores', array('siguelo' => 'SÍGUELO', 'contador' => $retadoresCount, 'actividad' => 'QUE ESTÁN APRENDIENDO', 'descripcion_actividad' => 'A TOCAR GUITARRA', 'followLink' => 'https://www.listerinesalud.com/reto21dias2015/public/index.php/users/follow/alberto', 'nombreRetador' => 'Alberto'))
@stop


@section('motivations')
@include('elements.content.motivacion', array('listerine_reto' => 'LO APOYÓ CON:', 'motivacion' => 'Porque me gustaría tocar guitarra y cantar en las reuniones familiares.', 'clases' => ' ¡21 CLASES DE GUITARRA!', 'urlVideo' => 'http://www.youtube.com/embed/9rbNQbBGu2E?rel=0&showinfo=0'))
@stop



@section('bitacora')
@include('elements.content.aprender', array('aprende_tambien' => 'APRENDE A TOCAR GUITARRA TU TAMBIÉN', 'retarAmigo' => 'Reta a un amigo a aprender a tocar guitarra en 21 Días'))
@stop

@section('lugares')
@include('elements.content.lugares', array('imagenLugar' => 'img/lugares-guitarra.png'))
@stop

@section('participantes')
@include('elements.content.participantes', array('participanteUnoFoto' => 'vir-img', 'participanteUnoCopy' => '“Todos en mi familia son amantes de la salsa, MI RETO es aprender a bailar salsa”', 'participanteUnoLink' => 'http://www.listerine.co/reto-21-dias/aprende-a-bailar-salsa', 'participanteDosFoto' => 'cesar-img', 'participanteDosCopy' => '“Sentirme y verme mejor, cambiar mis hábitos de comida y adoptar un estilo de vida saluble es MI RETO”', 'participanteDosLink' => 'http://www.listerine.co/reto-21-dias/una-vida-mas-saludable-en-21-dias', 'participanteTercerFoto' => 'jose-img', 'participanteTercerCopy' => '“El estrés diario y la rutina en mi trabajo quedarán atrás con MIS CLASES DE YOGA”', 'participanteTercerLink' => 'http://www.listerine.co/reto-21-dias/disminuye-el-estres'  ))
@stop

