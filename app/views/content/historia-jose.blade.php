@extends('story')


@section ('styles')
{{ HTML::style('css/jose_styles.css') }}
@stop

@section ('scripts')
{{ HTML::script('js/app_jose.js') }}
@stop


@section('intro')
@include('elements.content.perfil', array('reto' => 'MANEJAR EL ESTRÉS', 'reto_descripcion' => 'CON CLASES DE YOGA', 'youtube' =>'https://www.youtube.com/user/ListerineCO/', 'facebook' =>'https://www.facebook.com/ListerineCO/', 'instagram' =>'http://instagram.com/listerineco', 'twitter' =>'https://twitter.com/listerineco', 'fotoPerfil' => 'img/jose-perfil.png', 'tituloHabito' => 'DISMINUYE EL ESTRÉS'))
@stop


@section('numero')
@include('elements.content.retadores', array('siguelo' => 'SÍGUELO', 'contador' => $retadoresCount, 'actividad' => 'QUE ESTÁN MANEJANDO', 'descripcion_actividad' => 'EL ESTRÉS CON YOGA', 'followLink' => 'https://www.listerinesalud.com/reto21dias2015/public/index.php/users/follow/jose', 'nombreRetador' => 'Jose'))
@stop


@section('motivations')
@include('elements.content.motivacion', array('listerine_reto' => 'LO APOYÓ CON:', 'motivacion' => 'Porque me gustaría poder disfrutar las noches con mi familia después del trabajo sin estrés.', 'clases' => ' ¡21 SESIONES DE YOGA!', 'urlVideo' => 'https://www.youtube.com/embed/Afj46w5pDRo?rel=0&showinfo=0'))
@stop



@section('bitacora')
@include('elements.content.aprender', array('aprende_tambien' => 'MANEJAR EL ESTRÉS CON CLASES DE YOGA', 'retarAmigo' => 'Reta a un amigo a manejar el estrés con clases de yoga en 21 Días'))
@stop

@section('lugares')
@include('elements.content.lugares', array('imagenLugar' => 'img/lugares-guitarra.png'))
@stop

@section('participantes')
@include('elements.content.participantes', array('participanteUnoFoto' => 'vir-img', 'participanteUnoCopy' => '“Todos en mi familia son amantes de la salsa, MI RETO es aprender a bailar salsa”', 'participanteUnoLink' => 'http://www.listerine.co/reto-21-dias/aprende-a-bailar-salsa', 'participanteDosFoto' => 'beto-img', 'participanteDosCopy' => '“Siempre he querido tocar mi canción favorita en guitarra, esta es mi oportunidad”', 'participanteDosLink' => 'http://www.listerine.co/reto-21-dias/aprende-a-tocar-guitarra', 'participanteTercerFoto' => 'cesar-img', 'participanteTercerCopy' => '“Sentirme y verme mejor, cambiar mis hábitos de comida y adoptar un estilo de vida saluble es MI RETO”', 'participanteTercerLink' => 'http://www.listerine.co/reto-21-dias/una-vida-mas-saludable-en-21-dias'  ))
@stop

