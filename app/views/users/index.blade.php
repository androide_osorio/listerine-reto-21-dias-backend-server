@extends('admin-master')

@section('content')

    @if($users->isEmpty())
        <div class="jumbotron">
            <h4>You have no users</h4>
        </div>
    @endif
    @foreach($users as $user)
        <div class="user-info">
            <h4>{{ $user->fullName() }} <span class="text-success">{{ $user->type }}</span></h4>
            <p class="text-muted">{{ $user->dob }}</p>
            <p class="text-muted">{{ $user->email }}</p>
        </div>
    @endforeach
@stop
