@extends('master')

@section('content')
    <h4 id="tituloRecuadro" class="col-xs-12">
        <!--
        <strong>CUÉNTANOS TU RETO Y PARTICIPA
        POR GANAR EL APOYO DE LISTERINE®</strong>
        <br/><br/>
        -->
        <p>Buscamos a dos Colombianos que quieran el auspicio por
        $1'500.000 de LISTERINE® para cumplir su reto</p>
    </h4>

    <!--
    <div id="contLogoReto">
        <img src="img/logo-reto21dias.svg" alt="">
    </div>
    -->

    <div class="videoWrapper">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/YRyGNVNo_ZM" frameborder="0" allowfullscreen></iframe>
    </div>

    @if (Session::has('message'))
        <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
    @endif
    @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error => $message)
                {{ $message }}<br>
            @endforeach
        </div>
    @endif
    <div id="cuentaTuReto"><h1>CUÉNTANOS TU <strong>RETO</strong></h1></div>
    {{
        Form::open(array(
            'route' => 'users.store',
            'method' => 'POST',
            'id'=>"formularioReto",
            'role'=>"form",
            'data-toggle'=>"validator",
            'target'=>"_parent"
        ))
    }}
    @if (Request::hasCookie('block_user')==false)
        <div class="row">
            <div class="form-group col-xs-12 col-sm-6">
                <div class="label col-xs-12 col-sm-5 control-label">Nombre</div>
                {{
                    Form::input('text', 'name', null, array(
                    'class' => 'col-xs-12 col-sm-6 form-control',
                    'required'
                    ))
                }}
            </div>
            <div class="form-group col-xs-12 col-sm-6">
                <div class="label col-xs-12 col-sm-5 control-label">Apellido</div>
                {{
                    Form::input('text', 'last_name', null, array(
                    'class' => 'col-xs-12 col-sm-6 form-control',
                    'required'
                    ))
                }}
            </div>
            <div class="form-group col-xs-12 col-sm-6">
                <div class="label col-xs-12 col-sm-5 control-label">Email</div>
                {{
                    Form::input('email', 'email', null, array(
                    'class' => 'col-xs-12 col-sm-6 form-control',
                    'required'
                    ))
                }}
            </div>
            <div class="form-group col-xs-12 col-sm-6">
                <div class="label col-xs-12 col-sm-5 control-label" style="padding-top:0px; font-size: 17px;">Fecha de <br>nacimiento</div>
                <div class="">
                    <select name="day" class="col-xs-1 form-control dob_day" required>
                        <option value="" disabled selected>Día</option>
                        @for($i = 0; $i < 32; $i++)
                            <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                    </select>

                    <select name="month" class="col-xs-1 form-control dob_month" required>
                        <option value="" disabled selected>Mes</option>
                        <option value="1">enero</option>
                        <option value="2">febrero</option>
                        <option value="3">marzo</option>
                        <option value="4">abril</option>
                        <option value="5">mayo</option>
                        <option value="6">junio</option>
                        <option value="7">julio</option>
                        <option value="8">agosto</option>
                        <option value="9">septiembre</option>
                        <option value="10">octubre</option>
                        <option value="11">noviembre</option>
                        <option value="12">diciembre</option>
                    </select>

                    <select name="year" class="col-xs-2 form-control dob_year" required>
                        <option value="" disabled selected>Año</option>
                        @for($i = 1900; $i < 2016; $i++)
                            <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-5">
                <h2 class="control-label">YO QUIERO LOGRAR CON MI RETO:</h2>
                <p class="textoAyuda">(Asegúrate que tu RETO lO puedan tomar como ejemplo otras personas y te ayude a crear un hábito)</p>
            </div>
            <div class="col-sm-7">
                {{
                    Form::textarea('description',null,array(
                    'class'=>"form-control",
                    'style'=>"width:100%",
                    'rows'=>"6",
                    'required',
                    'pattern'=>".{10,155}"
                    ))
                }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-5">
                <h2 class="control-label">mi motivación para iniciar ESTE RETO es:</h2>
            </div>
            <div class="col-sm-7">
                {{
                    Form::textarea('motivation',null,array(
                    'class'=>"form-control",
                    'style'=>"width:100%",
                    'rows'=>"4",
                    'required',
                    'pattern'=>".{10,155}"
                    ))
                }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-5">
                <h2 class="control-label">QUIERO QUE LISTERINE ME AYUDE CON:</h2>
            </div>
            <div class="col-sm-7">
                {{
                     Form::textarea('help_request',null,array(
                     'class'=>"form-control",
                     'style'=>"width:100%",
                     'rows'=>"4",
                     'required',
                     'pattern'=>".{10,155}"
                     ))
                 }}
            </div>
        </div>

        <div class="row">

            <h4 class="adPermisos"  >Al marcar la casillas de permiso, usted acepta que podamos usar y publicar la información que brindó en la sección de arriba</h4>

            <div class="checkbox">
                <input type="checkbox" name="checkboxG1" id="checkboxG1" class="css-checkbox" data-error="Bruh, that email address is invalid" required/>
                <label for="checkboxG1" class="css-label" ></label>

                <p class="control-label">Usted está de acuerdo que la información que usted provea será administrada por Johnson and Johnson de Colombia S.A. de acuerdo con la <a>Política de Privacidad</a> de éste sitio.
                    <br><br>
                    Al hacer clic en el botón “Enviar", usted autoriza la cesión de su información a países fuera de su país de residencia, incluido a los Estados Unidos, los cuales podrán tener normas diferentes sobre la protección de datos que las vigentes en su países.</p>
            </div>

            <div class="help-block with-errors"></div>
            <div class="checkbox">
                <input type="checkbox" name="checkboxG2" id="checkboxG2" class="css-checkbox" data-error="Bruh, that email address is invalid" required/>
                <label for="checkboxG2" class="css-label"></label>
                <p class="control-label">¿Acepta que publiquemos su información en nuestro sitio de acuerdo a los <a href="http://www.listerine.co/terminos-y-condiciones-0">Términos y Condiciones</a> de éste sitio?</p>

            </div>
            <p class="text-muted" style="font-size:10px; margin-top:40px;">*Envía tu propio RETO DE TRANSFORMACIÓN a Johnson &amp; Johnson de Colombia S.A. a través de nuestra página web http://www.listerine.co/R21D a mas tardar el 27 de abril de 2015, y podrás ser elegido como una de las 2 historias de transformación que apoyará Johnson &amp; Johnson de Colombia S.A. con $1.500.000 cada una. Los retos se documentarán en video. La mera participación en la convocatoria para el apoyo económico y los documentales en video no garantiza que serás elegido como ganador. Johnson &amp; Johnson de Colombia S.A. seleccionará a su propia discreción y en forma subjetiva a las personas que apoyará económicamente y con las cuales hará los documentales. Las personas pre-seleccionadas deberán pasar primero por varias pruebas antes de recibir el aporte económico y en préstamo los equipos de filmación e iniciar el rodaje documentando su transformación. Las personas pre-seleccionadas también deberán suscribir todos los documentos legales diseñados por Johnson &amp; Johnson de Colombia S.A., que contienen la autorización de uso ilimitado a Johnson &amp; Johnson a nivel mundial del material sin ninguna compensación adicional en dinero o en especie, incluyendo la imagen de la persona. Solo podrán participar mayores de edad.</p>
            <p class="text-muted" style="font-size:10px; margin-top:40px;">*Si es publicada, su declaración estará disponible para los usuarios del Sitio y podrá estar disponible para el público, así que no incluya ninguna información personal en ella. Utilizaremos la información que envíe de acuerdo con nuestra <a href="http://www.listerine.co/politica-de-privacidad">Política de Privacidad</a>. Al presentar una declaración, usted reconoce que ha revisado y acepta nuestra <a href="http://www.listerine.co/politica-de-privacidad">Política de Privacidad</a>.</p>
        </div>

        <div class="form-group">
            <div class="col-xs-12" style="text-align:center;">
                {{ Form::submit('REGISTRARSE', array('class' => 'btn btn-default', 'id' => 'enviar')) }}
            </div>
        </div>
    @else
        <div class="alert alert-danger">
            <p>Para poder participar en el concurso, debe ser mayor de dieciocho (18) años</p>
        </div>
    @endif
    {{ Form::close() }}
@stop

@section('scripts')
<script>
    $('#formularioReto').validator()
</script>
@stop
