@extends('master')

@section('content')
    <div id="cuentaTuReto"><h1>DEJAR DE SEGUIR EL <strong>RETO</strong> DE {{$historia}}</h1></div>
    {{
        Form::open(array(
            'route' => 'users.unfollow',
            'method' => 'POST',
            'id'=>"formularioReto",
            'role'=>"form",
            'data-toggle'=>"validator"
        ))
    }}
    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    {{ $error }}<br>
                @endforeach
            </div>
        @endif

    </div>
    @if (Session::has('message')==false)
        <p class="copy" style="margin: 10px auto 50px auto; padding: 0px 30px;">Tres personas más están transformando su
            vida con el Reto 21 Días de LISTERINE®. Síguelas y entérate de ellas.</p>

        <input type="hidden" value="{{$email}}" name="email" />
        <input type="hidden" value="{{$historia}}" name="historia" />

        <div class="form-group">
            <div class="col-xs-12" style="text-align:center;">
                {{ Form::submit('DEJAR DE SEGUIR', array('class' => 'btn btn-default', 'id' => 'enviar')) }}
            </div>
        </div>
    @else
        <div class="row">
            <p class="copy" style="margin: 10px auto 50px auto; padding: 0px 30px;">Tres personas más están
                transformando su vida con el Reto 21 Días de LISTERINE®. Síguelas y entérate de ellas.</p>

            <div class="group">
                @if($historia!="alberto")
                    <a href="http://www.listerine.co/reto-21-dias/aprende-a-tocar-guitarra">
                        <div class="participante">
                            <div class="nail beto"></div>
                            <div class="copy">“¡Por fin! Iniciando las clases”</div>
                            <div class="habit-beto"></div>
                        </div>
                    </a>
                @endif
                @if($historia!="virginia")
                    <a href="http://www.listerine.co/reto-21-dias/aprende-a-bailar-salsa">
                        <div class="participante">
                            <div class="nail virginia"></div>
                            <div class="copy" style="padding: 1px;">“No era tan mala como pensaba”</div>
                            <div class="habit-virginia"></div>
                        </div>
                    </a>
                @endif
                @if($historia!="cesar")
                    <a href="http://www.listerine.co/reto-21-dias/una-vida-mas-saludable">
                        <div class="participante">
                            <div class="nail cesar"></div>
                            <div class="copy">“8o día y me efrento a la pesa”</div>
                            <div class="habit-cesar"></div>
                        </div>
                    </a>
                @endif
                @if($historia!="jose")
                    <a href="http://www.listerine.co/reto-21-dias/manejar-el-estres">
                        <div class="participante">
                            <div class="nail jose"></div>
                            <div class="copy">“¡Terminé! Y Soy el más en yoga”</div>
                            <div class="habit-jose"></div>
                        </div>
                    </a>
                @endif
            </div>
        </div>
    @endif
    {{ Form::close() }}
@stop

@section('scripts')
    <script>
        $( '#formularioReto' ).validator()
    </script>
@stop