<nav class="" style="background-color:#1FBECA;" role="navigation">
  <div class="container">
    <div class="nav-wrapper">
      <a id="logo-container" href="#" class="brand-logo" style="  margin-top: 6px; height: 50px;">
        <img style="height:100%;" src="../img/logo-reto21dias_white.svg" alt="">
      </a>
      <ul class="right">
        <li ng-class="{active:isActive('/')}">
          <a href="#/"><i class="mdi-action-dashboard left"></i>Dashboard</a>
        </li>
        <li ng-class="{active:isActive('/retos')}">
          <a href="#/retos"><i class="mdi-social-group left"></i>Retos</a>
        </li>
        <li ng-class="{active:isActive('/email-marketing')}">
          <a href="#/email-marketing"><i class="mdi-editor-format-list-bulleted left"></i>Listas de email</a>
        </li>
      </ul>
    </div>
  </div>
</nav>