<div class="contenedor">
    <div class="laMotivacion">
        <div class="titulo-motivacion">
            <div class="arrow-right" style="margin-top: 15px;"></div>
            <span class="title-motivacion fuente bold" style="font-size:35px;">MOTIVACIÓN:</span>
        </div>
        <center>
            <div class="comilla fuente bold" style="margin-right:-4px; font-size:46px; color:white; padding-left:3px;">
                “
            </div>
            <div class="frase valign-wrapper">
                <p class="valign">{{ $motivacion }}</p>
            </div>
            <div class="comilla fuente bold" style="margin-left:-4px; font-size:46px; color:white; padding-left:3px;padding-top:58px;vertical-align:text-bottom;">
                ”
            </div>
            <div class="content-video" style="width: 96.5%; border: 5px solid #008f99; margin-top: -3px; height:410px;">
                <iframe class="youtube-player" width="100%" height="400px" src="{{ $urlVideo }}" allowfullscreen
                        frameborder="0" style="border:none;"></iframe>
            </div>
        </center>
    </div>
    <div class="listerine-apoyo">
        <div class="titulo-motivacion">
            <div class="arrow-right two" style="margin-top: 15px;"></div>
            <div class="arrow-down"></div>
            <span class="title-motivacion fuente bold" style="font-size:35px;">LISTERINE<sup style="font-size: 12px; top: -1.5em !important;">®</sup></span>
            <span class="span-block fuente bold"> {{ $listerine_reto}}</span>
        </div>
        <div class="box-apoyo fuente">
            {{-- <div class="imagen-apoyo"></div> --}}
            <p class="ok-clases">{{ $clases }}</p>
        </div>
    </div>
</div>