<div class="titulo-aprende">
	<div class="contenedor" style="display:inline-block;">
		<div class="arrow-down"></div>
		<div class="arrow-right two" style="margin-top:7px; margin-right:15px;"></div>
		<p class="fuente bold aprende-salsa" style="font-size: 27px !important; margin-top: 10px;">{{ $aprende_tambien }}</p>
	</div>
</div>
<div class="shadow"></div>

<div class="contenedor" >
	<div class="retar">
		<p class="fuente" style="margin-top:20px;"><span class="light">{{ $retarAmigo}}</span></p>
		<a href="https://api.addthis.com/oexchange/0.8/forward/facebook/offer?url=http%3A%2F%2Fwww.listerine.co%2F&pubid=ra-54e611817fbf1040&ct=1&title=Te%20reto%20a%20bailar%20salsa%20en%2021%20d%C3%ADas&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/facebook.png" border="0" alt="Facebook"/></a>

		<a href="https://api.addthis.com/oexchange/0.8/forward/twitter/offer?url=http%3A%2F%2Fwww.listerine.co%2F&pubid=ra-54e611817fbf1040&ct=1&title=Te%20reto%20a%20bailar%20salsa%20en%2021%20d%C3%ADas&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/twitter.png" border="0" alt="Twitter"/></a>

		<a href="https://api.addthis.com/oexchange/0.8/forward/email/offer?url=http%3A%2F%2Fwww.listerine.co%2F&pubid=ra-54e611817fbf1040&ct=1&title=Te%20reto%20a%20bailar%20salsa%20en%2021%20d%C3%ADas&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/email.png" border="0" alt="Email"/></a>
	</div>

	<center><img src="http://www.listerine.co/reto21dias2015/bitacora/virginia/img/listerine-reto.png" width="80%" style="margin-top:37px;"/></center>
	<br><br>

	
	<div class="angularjS" ng-app="myApp">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" ng-controller="ParticipanteController">

			<div class="panel panel-default dia-info" ng-repeat="dia in dias">
				<div class="panel-heading @{{dia.animated}}" role="tab" id="headingOne" ng-class="@{{dia.clase}}" style="background-color:@{{dia.color}};">

					<a data-toggle="collapse" data-parent="#accordion" href="#collapse@{{ $index + 1 }}" aria-expanded="@{{dia.abierto}}" aria-controls="collapse@{{ $index + 1 }}"><h4 class="panel-title" style="color:white;">
						@{{dia.name}}
						<span class="servicedrop glyphicon glyphicon-plus pull-right"></span>
					</h4></a>

				</div>

				<div id="collapse@{{ $index + 1 }}" class="panel-collapse collapse @{{dia.colapse}}" role="tabpanel" aria-labelledby="headingOne" >
					<div class="panel-body" style="background-color:@{{dia.color}};">

						<script>
						$('.collapse').on('shown.bs.collapse', function(){
							$(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
							$('.panel-heading').removeClass("animated pulse infinite");
						}).on('hidden.bs.collapse', function(){
							$(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
							$('.panel-heading').removeClass("animated pulse infinite");
						});
						</script>


						<div class="dia-retador siempre valign-wrapper" style="display:none;">
							<div class="valign">
								<span class="fuente" style="font-size: 34px; line-height: 27px;">DÍA<br><span style="font-size: 110px; line-height: 84px;">@{{dia.dia}}</span></span>
							</div>
						</div>

						<div class="contenido-dia-participante">
							<div class="fotoDelDia">
								<div class="foto-dia" ng-style="{'background-image':'url('+dia.foto+')'}" style="display:none;"></div>
								<iframe class="youtube-player" width="100%" height="250" ng-src="@{{trustSrc(dia.video)}}" allowfullscreen frameborder="0" ></iframe>
							</div>

							<div class="listerineDia">
								<div class="listerine-dia-reto" ng-style="{'background-image':'url('+dia.fotoBotella+')'}">
									<div class="copy-listerine-dia fuente light">
										@{{dia.listerine}}</div>
									</div>
									<div class="check-listerine">
										<p style="font-size: 13px; line-height: 13px;">@{{dia.reto}}</p>
										<div>

											<a href="https://api.addthis.com/oexchange/0.8/forward/facebook/offer?url=http%3A%2F%2Fwww.listerine.co%2F&pubid=ra-54e611817fbf1040&ct=1&title=Te%20reto%20a%20bailar%20salsa%20en%2021%20d%C3%ADas&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/facebook.png" border="0" alt="Facebook"/></a>

											<a href="https://api.addthis.com/oexchange/0.8/forward/twitter/offer?url=http%3A%2F%2Fwww.listerine.co%2F&pubid=ra-54e611817fbf1040&ct=1&title=Te%20reto%20a%20bailar%20salsa%20en%2021%20d%C3%ADas&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/twitter.png" border="0" alt="Twitter"/></a>

											<a href="https://api.addthis.com/oexchange/0.8/forward/email/offer?url=http%3A%2F%2Fwww.listerine.co%2F&pubid=ra-54e611817fbf1040&ct=1&title=Te%20reto%20a%20bailar%20salsa%20en%2021%20d%C3%ADas&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/email.png" border="0" alt="Email"/></a>
										</div>
										<script>
										</script>
									</div>	
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<style>
.panel-heading.animated.pulse.infinite{
	-webkit-animation-duration: 3s !important;
}
</style>


