<div class="titulo-retos">
    <div class="contenedor-logo">
        <div class="logo-footer"></div>
    </div>
    <div class="sigue-transformaciones">
        <center>
            <p class="fuente">
                <span class="bold historia">SIGUE OTRAS HISTORIAS</span><br>
                <span class="light siguelo">DE TRANSFORMACIÓN REAL Y ÚNETE A SU RETO</span>
            </p>
        </center>
    </div>
</div>
<div class="shadow"></div>
<div class="participante-footer">
    <div class="height-parti" style="width:100%; height: 18px;"></div>
    <a href="{{ $participanteUnoLink}}" target="_parent">
        <div class="{{ $participanteUnoFoto }}"></div>
    </a>

    <div class="copy-participante"><p class="fuente bold" style="color:#26b9c5;">{{ $participanteUnoCopy}}</p></div>
    <a href="{{ $participanteUnoLink}}" target="_parent">
        <div class="btn-footer fuente">
            <span class="light">SIGUE ESTA HISTORIA<br>Y ÚNETE A</span>
            <span class="bold">SU RETO</span>
        </div>
    </a>
</div>
<div class="participante-footer" style="margin-left:-7px;">
    <a href="{{ $participanteDosLink}}" target="_parent">
        <div class="{{ $participanteDosFoto }}" style="margin-top:22px;"></div>
    </a>

    <div class="copy-participante"><p class="fuente bold" style="color:#26b9c5;">{{ $participanteDosCopy }}</p></div>
    <a href="{{ $participanteDosLink}}" target="_parent">
        <div class="btn-footer fuente">
            <span class="light">SIGUE ESTA HISTORIA<br>Y ÚNETE A</span>
            <span class="bold">SU RETO</span>
        </div>
    </a>
</div>
<div class="participante-footer" style="margin-left:-7px;">
    <a href="{{ $participanteTercerLink}}" target="_parent">
        <div class="{{ $participanteTercerFoto }}"></div>
    </a>

    <div class="copy-participante"><p class="fuente bold" style="color:#26b9c5;">{{ $participanteTercerCopy }}</p></div>
    <a href="{{ $participanteTercerLink}}" target="_parent">
        <div class="btn-footer fuente">
            <span class="light">SIGUE ESTA HISTORIA<br>Y ÚNETE A</span>
            <span class="bold">SU RETO</span>
        </div>
    </a>
</div>