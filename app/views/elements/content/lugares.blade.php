<div class="contenedor">
	<center>
		{{HTML::image( $imagenLugar, 'Acepta el reto Listerine 21 días', array('class' => 'margin-logo', 'width' => '100%' , 'alt' => 'Lugares Salsa en Cali'));}}
{{-- 		<img src="img/recomendar-salsa.png" width="100%" alt="Lugares Salsa en Cali" style="margin-top:40px;"/> --}}</center>
	<div class="publicar-lugar fuente bold">¿QUIERES PUBLICAR ALGÚN LUGAR EN ESTA LISTA?</div>
	<div class="button-lugares fuente light">HAZ CLIC AQUÍ</div>

	<div class="slider-pc">
		<div class="slick-class" style="margin-top:25px;">
			<div style="color:#1b5265">
				<h3 class="fuente bold">EL MANICERO</h3><br>
				<p class="fuente"><span class="bold">El Manicero</span> <span class="light">está situado en un lugar muy céntrico, justo a la salida de la estación del metro-bus Tequendama. El Manicero es una especie de escuela social, lo que significa que los precios son muy atractivos.</span></p>
				<p class="fuente"><span class="bold">Precio:</span><span class="light"> $7.000 2 horas</span></p>
				<p class="fuente"><span class="bold">Dirección:</span><span class="light"> Calle 5 # 39-71 Cali, Colombia</span></p>

			</div>
			<div style="color:#1b5265">
				<h3 class="fuente bold">SON DE LUZ</h3><br>
				<p class="fuente"><span class="bold">Luz</span> <span class="light"> es la propietaria e instructora principal en la escuela, tiene mucha experiencia y puede detectar los errores en un instante. Luz es una de las mejores profesoras de Salsa que puedes tener en Cali.</span></p>
				<p class="fuente"><span class="bold">Precio:</span><span class="light"> $30.000 1 hora Particular</span></p>
				<p class="fuente"><span class="bold">Dirección:</span><span class="light"> Calle 7 N0 27-32</span></p>
				<p class="fuente"><span class="bold">Teléfono:</span><span class="light"> +572 557 0851</span></p>
			</div>
{{-- 			<div><h3>3</h3></div>
			<div><h3>4</h3></div>
			<div><h3>5</h3></div>
			<div><h3>6</h3></div>
			<div><h3>7</h3></div>
			<div><h3>8</h3></div>
			<div><h3>9</h3></div>
			<div><h3>10</h3></div> --}}
		</div> 

	</div>

	<div class="slider-mobile">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<div class="carousel-caption" style="color:#1b5265">
						<h3 class="fuente bold">EL MANICERO</h3><br>
						<p class="fuente"><span class="bold">El Manicero</span> <span class="light">está situado en un lugar muy céntrico, justo a la salida de la estación del metro-bus Tequendama. El Manicero es una especie de escuela social, lo que significa que los precios son muy atractivos.</span></p>
						<p class="fuente"><span class="bold">Precio:</span><span class="light"> $7.000 2 horas</span></p>
						<p class="fuente"><span class="bold">Dirección:</span><span class="light"> Calle 5 # 39-71 Cali, Colombia</span></p>
					</div>
				</div>

				<div class="item">
					<div class="carousel-caption" style="color:#1b5265">
						<h3 class="fuente bold">SON DE LUZ</h3><br>
						<p class="fuente"><span class="bold">Luz</span> <span class="light"> es la propietaria e instructora principal en la escuela, tiene mucha experiencia y puede detectar los errores en un instante. Luz es una de las mejores profesoras de Salsa que puedes tener en Cali.</span></p>
						<p class="fuente"><span class="bold">Precio:</span><span class="light"> $30.000 1 hora Particular</span></p>
						<p class="fuente"><span class="bold">Dirección:</span><span class="light"> Calle 7 N0 27-32</span></p>
						<p class="fuente"><span class="bold">Teléfono:</span><span class="light"> +572 557 0851</span></p>
					</div>
				</div>
			</div>

			<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
			<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
		<script>
		$(document).ready(function(){
			$('.slick-class').slick({
				infinite: true,
				slidesToShow: 2,
				speed: 200,
				slidesToScroll: 2
			});
		});
		</script>
		
</div>