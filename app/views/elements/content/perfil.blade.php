<div class="introduccion-reto">
	<div class="titulo-reto" style="display:none;">{{ $tituloHabito }}</div>

	<div class="height-intro valign-wrapper logo-height">
		{{HTML::image('img/logo-reto21dias.svg', 'Acepta el reto Listerine 21 días', array('id' => 'logo-reto', 'width' => 74 , 'height' => 111));}}

	</div>

	<div class="height-intro perfil-apoyo">
		<center>
			<div class="personal-information">
				{{HTML::image('img/listerine-apoya.png', 'Acepta el reto Listerine 21 días', array('class' => 'listerine-apoya', 'width' => '80%'));}}
				{{HTML::image($fotoPerfil, 'Acepta el reto Listerine 21 días', array('class' => 'listerine-fotoPerfil'));}}
			</div>
		</center>
	</div>

	<div class="height-intro el-reto-de">
		<div class="tituloReto">
			<center>
				<span class="fuente bold" style="font-size:22px; color:#59babf;">EN SU RETO DE</span><br>
				<span class="fuente bold bailar-salsita">{{ $reto }}</span><br>
				<span class="fuente light profe" style="font-size:21px; color:#008f99;">{{ $reto_descripcion}}</span>
			</center>
		</div>
		<div class="imagen-zapatos"></div>
		<div class="sigue-historia">
			<div class="contacto">
				<div class="siguela fuente light" style="margin-top:-4px;">Sigue esta<br>historia</div>
				<a href="{{ $youtube }}" target="_blank"><div class="youtubeChannel"></div></a>
				<a href="{{ $facebook}}" target="_blank"><div class="facebookChannel"></div></a>
				<a href="{{ $instagram}}" target="_blank"><div class="instagramChannel"></div></a>
				<a href="{{ $twitter}}" target="_blank"><div class="twitterChannel"></div></a>
				<!-- <div class="siguela fuente bold" style="margin-top:5px;">/LISTERINECO</div> -->
			</div>
		</div>
	</div>
</div>