<?php

/**
 * Created by androide_osorio.
 * Date: 3/11/15
 * Time: 13:29
 */
class EmailEventHandler {

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher $events
     *
     * @return array
     */
    public function subscribe($events)
    {
        $events->listen( 'email.register', 'EmailEventHandler@onConsumerRegister' );

        $events->listen( 'email.follow', 'EmailEventHandler@onConsumerFollow' );

        $events->listen( 'email.register-pro', 'EmailEventHandler@onProfessionalReferring' );
    }

    /**
     * sends email to the consumer once he/she successfully registered
     *
     * @param $emailData
     */
    public function onConsumerRegister($emailData)
    {
        $view = 'emails.reto-register';
        $subject = 'Gracias por unirse al reto';

        $this->sendEmail( $emailData, $subject, $view );
        Log::info("Email Sent. Event: consumer subscribed", $emailData);
    }

    /**
     * sends email to the consumer once it started to follow a story
     *
     * @param $emailData
     */
    public function onConsumerFollow($emailData)
    {
        $view = 'emails.follow';
        $subject = 'Ahora estás siguiendo el reto de ' . $emailData[ "following" ];

        $this->sendEmail( $emailData, $subject, $view );
        Log::info("Email Sent. Event: consumer followed story", $emailData);
    }

    /**
     * sends email to professional about the referral and the teaser email
     * to the referred consumer
     *
     * @param $emailData
     */
    public function onProfessionalReferring($emailData, $professional_id)
    {
        $professional = Professional::find( $professional_id );
        $view = 'emails.EM-odontologo';
        $subject = 'Gracias por recomendarle el reto 21 dias a tu paciente';

        $professionalEmailData = array(
            'name'           => $professional->fullName(),
            'email'          => $professional->email,
            'consumer_name'  => $emailData[ 'name' ],
            'consumer_email' => $emailData[ 'email' ]
        );

        $this->sendEmail( $professionalEmailData, $subject, $view );

        //-----------------

        $view = 'emails.EM-paciente';
        $subject = 'Unete al reto 21 dias de LISTERINE®';

        $emailData['professional'] = $professional->fullName();
        $this->sendEmail( $emailData, $subject, $view );
        Log::info("Email Sent. Event: professional referred consumer through iPhone app", array($professionalEmailData, $emailData));
    }

    //---------------------------------------------------
    /**
     * sends an email with the specified parameters
     *
     * @param $data
     * @param $subject
     * @param $viewName
     */
    protected function sendEmail($data, $subject, $viewName)
    {
        Mail::send( $viewName, $data, function ($message) use ($data, $subject)
        {
            $email = isset($data['email']) ? $data['email'] : $data['user_email'];
            $name = isset($data['name']) ? $data['name'] : '';

            $message->from( "no-reply@listerinesalud.com", "LISTERINE®" );
            $message->to( $email, $name )
                    ->subject( $subject );
        } );
    }
}