<?php
/**
 * Created by androide_osorio.
 * Date: 3/25/15
 * Time: 14:16
 */
use Guzzle\Http\Client as GuzzleClient;

class ProfessionalsEventHandler {

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher $events
     *
     * @return array
     */
    public function subscribe($events)
    {
        $events->listen( 'professionals.created', 'ProfessionalsEventHandler@onProfessionalCreated' );
    }

    /**
     * sends email to the consumer once he/she successfully registered
     *
     * @param $professional
     *
     * @internal param $emailData
     */
    public function onProfessionalCreated(Professional $professional, $password, $pass_confirm)
    {
        $client = new GuzzleClient();
        $gender = $professional->gender == 'M' ? 'Masculino' : 'Femenino';

        $info = array(
            'name'      => $professional->email,
            'pass'      => $password,
            'mail'      => $professional->email,
            'init'      => $professional->email,
            'field_user_country_id' => 16,
            'field_first_name' => $professional->name,
            'field_last_name' => $professional->last_name,
            'field_gender' => $gender,
            'field_d_o_b' => $professional->dob->format('c'),
            'field_phone_number' => $professional->phone
        );

        $request = $client->post('https://listerinesalud.com/api/registrar/profesional',array(
            'content-type' => 'application/json'
        ),array());

        $data = json_encode($info);
        $request->setBody($data);
        $request->send();
    }
}