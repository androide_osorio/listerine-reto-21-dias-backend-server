<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//render the subscription form at homepage
Route::get( '/', 'UsersController@create');


// Display the correct challenger journal according to the passed in URL
Route::get( '/historias/{historia}', 'StoriesController@show' );

/*
|--------------------------------------------------------------------------
| Public Routes
|--------------------------------------------------------------------------
 */

/*
 * User Resource
 * only include index, create and store actions
 */
Route::resource( 'users', 'UsersController', array( 'only' => array( 'index', 'create', 'store' ) ) );

Route::group(array( 'prefix' => 'users' ), function()
{
    //performs the follow process in the database linked in the controller
    Route::post( '/follow', array(
        'as'   => 'users.follow',
        'uses' => 'FollowController@follow'
    ) );

    //renders the form that asks the user his/her email for following
    Route::get( '/ /{historia}', array(
        'as'   => 'users.showFollow',
        'uses' => 'FollowController@showFollow'
    ) );

    /*
     * Unfollow Routes
     */
    //show the confirmation page for unfollowing a story for the passed in user
    Route::get( '/unfollow', array(
        'as'   => 'users.showUnfollow',
        'uses' => 'FollowController@showUnfollow'
    ) );

    //perform the unfollow process linked to a controller method
    Route::post( '/unfollow', array(
        'as'   => 'users.unfollow',
        'uses' => 'FollowController@unfollow'
    ) );
});


//-------------------------------------------------------------------------------
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
 */

/*
 * group all the API-related routes un a route group. All the controllers of
 * this group should be placed in the Api Namespace
 */
Route::group( array( 'prefix' => 'api', 'namespace' => 'Api'), function () {

    Route::get( 'authenticate', array(
        'uses' => 'AuthenticationController@index',
        'as' => 'api.auth.token.index'
    ));
    Route::post( 'authenticate', array(
        'uses' => 'AuthenticationController@store',
        'as' => 'api.auth.token.generate',
        //'before' => 'errors'
    ));

    Route::resource( 'professionals', 'ProfessionalsController', array(
        'only' => array( 'index', 'store', 'show' )
    ) );

    Route::resource('users', 'UsersController', array(
        'only' => array( 'index', 'show' )
    ));

    Route::resource('challenges', 'ChallengesController', array(
        'only' => array( 'index', 'show' )
    ));

    Route::resource('follow_user', 'FollowController', array(
        'only' => array( 'index', 'show' )
    ));

    Route::get('follows/{following}/stats/{periodicity}', array(
            'as' => 'api.follows.stats',
            'uses' => 'FollowController@follower_stats')
    );

    Route::get('users/{type}/stats/{periodicity}', array(
            'as' => 'api.users.stats',
            'uses' => 'UsersController@user_stats')
    );

    Route::get( '/users/metrics', 'UsersController@metrics');

    Route::group( array( 'before' => 'auth.token' ), function () {

        Route::delete( 'authenticate', array(
            'uses' => 'AuthenticationController@destroy',
            'as' => 'api.auth.token.destroy'
        ));

        Route::post( 'consumers/engage', array(
            'uses'   => 'ConsumersController@engage',
            'as'     => 'consumers.engage'
        ) );
    } );
} );

//-------------------------------------------------------------------------------
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
 */

$adminRoutesOptions = array(
    //'before'    => 'auth',
    'namespace' => 'Admin',
    'prefix'    => 'admin'
);

/*
 * group all the API-related routes un a route group. All the controllers of
 * this group should be placed in the Api Namespace
 */
Route::group( $adminRoutesOptions, function ()
{
    Route::get('/', function()
    {
        return View::make( 'admin/home-admin' );
    });

    Route::resource('dashboard', 'DashboardController');


} );

Route::get('/export_retos', function() {
    $table = Consumer::with('challenge')->get();

    $output = implode("/", array('Nombre', 'Apellido', 'Fecha de nacimiento', 'Email', 'Descripción del reto', 'Motivacion para hacer el reto','Que necesita para el reto'));
    $output .="\n";     //Adding New Line

    foreach ($table as $row) {
        $output .=  implode("/", array($row['name'],$row['last_name'],$row['dob'],$row['email'],$row['challenge']['description'],$row['challenge']['motivation'],$row['challenge']['help_request'])); // append each row
        $output .="\n";     //Adding New Line
    }

    $headers = array(
        'Content-Type' => 'text/csv',
        'Content-Disposition' => 'attachment; filename="retos.csv"',
    );

    return Response::make(rtrim($output, "\n"), 200, $headers);
});