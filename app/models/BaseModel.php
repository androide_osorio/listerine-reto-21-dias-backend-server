<?php

/**
 * Created by androide_osorio.
 * Date: 3/9/15
 * Time: 09:58
 */
class BaseModel extends Ardent {

    public function save(array $rules = array(),
                         array $customMessages = array(),
                         array $options = array(),
                         Closure $beforeSave = null,
                         Closure $afterSave = null
    )
    {
        $rules = $this->mergeRules();
        return parent::save( $rules, $customMessages, $options, $beforeSave, $afterSave );
    }

    public function validate(array $rules = array(), array $customMessages = array())
    {
        $mergedRules = $this->mergeRules();
        return parent::validate( $mergedRules, $customMessages );
    }

    /**
     * Merge Rules
     *
     * Merge the rules arrays to form one set of rules
     */
    protected function mergeRules()
    {
        $output = array();
        $merged = array();

        if ( $this->exists ) {
            $merged = array_merge_recursive( static::$rules[ 'save' ], static::$rules[ 'update' ] );
        }
        else {
            $merged = array_merge_recursive( static::$rules[ 'save' ], static::$rules[ 'create' ] );
        }

        foreach ( $merged as $field => $rules ) {
            if ( is_array( $rules ) ) {
                $output[ $field ] = implode( "|", $rules );
            }
            else {
                $output[ $field ] = $rules;
            }
        }

        return $output;
    }
}