<?php

/**
 * Represents the specific user role for challengers who want to gain
 * LISTERINE's support
 * Class Consumer
 */
class Consumer extends User {

	/* ------------------------------------ *
	 * Model Relationships
	 * ------------------------------------ */

	/**
	 * a challenger user can have only ONE challenge registered
	 * @return mixed
	 */
	public function challenge()
	{
		return $this->hasOne('Challenge', 'user_id');
	}

	/**
	 * saves a challenge in database in behalf of the current user
	 *
	 * @param array $data
	 *
	 * @return \Challenge
	 */
	public function saveChallenge( array $data )
	{
		$challenge = new Challenge;
		$challenge->fill( $data );
		$challenge->user_id = $this->id;

		if($challenge->validate()) {
			$challenge->save();
		}

		return $challenge;
	}
}