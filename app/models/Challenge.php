<?php

/**
 * represents a single challenge request that will participate to win the contest
 * Class Challenge
 */
class Challenge extends Ardent {

	/**
	 * validation rules
	 * @var array
	 */
	public static $rules = array(
		'description'  => 'required|min:7',
		'motivation'   => 'required|min:7',
		'help_request' => 'required|min:7'
	);

	/**
	 * fillable attributes with mass assignment
	 * @var array
	 */
	protected $fillable = array('description', 'motivation', 'help_request', 'featured');

	//-----------------------------------------------
	/* ------------------------------------ *
	 * Model Relationships
	 * ------------------------------------ */

	/**
	 * a challenge must belong to a registered user
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}
}