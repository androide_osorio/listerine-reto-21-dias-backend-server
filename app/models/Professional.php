<?php

/**
 * represents an Oral Health care professional registered
 * in listerine.co
 * Class Professional
 */
class Professional extends User {
    /**
     * validation rules
     *
     * @var array
     */
    public static $rules = array(
        'create' => array(
            'dob'      => 'before:-18 years',
            'email'    => 'email|unique:users,email',
            'phone'    => 'required',
            'gender'   => 'in:M,F',
            'password' => 'required|min:8|confirmed'
        ),
        'save'   => array(
            'name'      => 'required',
            'last_name' => 'required',
            'dob'       => 'required',
            'email'     => 'required',
        ),
        'update' => array()
    );

    /**
     * custom error messages
     *
     * @var array
     */
    public static $customMessages = array(
        'required'           => 'El campo :attribute es requerido',
        'email'              => 'El campo :attribute debe ser un correo electronico valido',
        'min'                => 'El campo :attribute debe contener mas de ...',
        'dob.before'         => 'debes ser mayor de edad (+18) para enviar este formulario',
        'email.unique'       => 'La dirección de correo electrónico ya existe',
        'required'           => 'El campo :attribute es requerido',
        'password.min'       => 'La contraseña que escribió es demsiado corta (minimo 8 caracteres)',
        'password.confirmed' => 'La contraseña no coincide con la confirmación. Intentela escribirlas de nuevo.'
    );

    public $hidden = array( 'password', 'token' );
}