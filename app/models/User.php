<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * Generic class for users that can be diversified into different types of user
 * Class User
 */
class User extends BaseModel implements UserInterface, RemindableInterface {

    /**
     * validation rules
     *
     * @var array
     */
    public static $rules = array(
        'create' => array(
            'dob'          => 'before:-18 years',
            'email'        => 'email|unique:users,email',
        ),
        'save' => array(
            'name'         => 'required',
            'last_name'    => 'required',
            'dob'          => 'required',
            'email'        => 'required',
        ),
        'update' => array()
    );

    /**
     * custom error messages
     * @var array
     */
    public static $customMessages = array(
        'required'     => 'El campo :attribute es requerido',
        'email'        => 'El campo :attribute debe ser un correo electronico valido',
        'min'          => 'El campo :attribute debe contener mas de ...',
        'dob.before'   => 'debes ser mayor de edad (+18) para enviar este formulario',
        'email.unique' => 'La dirección de correo electrónico ya existe'
    );

    /**
     * attributes protected from mass assignment
     *
     * @var array
     */
    protected $guarded = array( 'id', 'type' );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The name to save in the type field
     *
     * @var string
     */
    protected static $base_type = 'User';

    /**
     * The name of the colum that holds table_type
     *
     * @var string
     */
    protected static $table_type_field = 'type';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
	protected $hidden = array('password');

    /**
     * defines password attributes
     * @var array
     */
    public static $passwordAttributes  = array('password');

    /**
     * auto-encrypt password values in database
     * @var bool
     */
    public $autoHashPasswordAttributes = true;

    /**
     * purge redundant attributes like password confirmations
     * @var bool
     */
    public $autoPurgeRedundantAttributes = true;

    //-------------------------------------------------------------------------
    /* ------------------------------------ *
     * Single Table inheritance
     * ------------------------------------ */
    /**
     * override constructor to support single table inheritance
     *
     * @param array $attributes
     */
    public function __construct($attributes = array())
    {
        parent::__construct( $attributes );
        if ( $this->useSti() ) {
            $this->setAttribute( self::$table_type_field, get_class( $this ) );
        }
    }

    /**
     * returns true if this model uses single table inheritance
     *
     * @return bool
     */
    private function useSti()
    {
        return ( self::$table_type_field && self::$base_type );
    }

    /**
     * @param bool $excludeDeleted
     *
     * @return mixed
     */
    public function newQuery($excludeDeleted = true)
    {
        $builder = parent::newQuery( $excludeDeleted );
        // If I am using STI, and I am not the base class,
        // then filter on the class name.
        if ( $this->useSti() && get_class( new self::$base_type ) !== get_class
            ( $this )
        ) {
            $builder->where( self::$table_type_field, "=", get_class( $this ) );
        }

        return $builder;
    }

    /**
     * @param array $attributes
     *
     * @return mixed
     */
    public function newFromBuilder($attributes = array())
    {
        if ( $this->useSti() && $attributes->{self::$table_type_field} ) {
            $class = $attributes->{self::$table_type_field};
            $instance = new $class;
            $instance->exists = true;
            $instance->setRawAttributes( (array)$attributes, true );

            return $instance;
        }
        else {
            return parent::newFromBuilder( $attributes );
        }
    }

    //-------------------------------------------------------------------------
    /* ------------------------------------ *
     * Helper Methods
     * ------------------------------------ */

    /**
     * merges base user validation rules with the specific ones for the class
     * that´s been currently queried
     * @return array
     */
    public static function mergeValidationRules()
    {
        $baseRules = User::$rules;
        $classRules = self::$rules;

        return array_merge($baseRules, $classRules);
    }

    /**
     * merges base user validation error messages according to the base class and the current one
     * @return array
     */
    public static function mergeErrorMessages()
    {
        $baseMessages = User::$error_messages;
        $classMessages = self::$error_messages;

        return array_merge($baseMessages, $classMessages);
    }



    /**
     * retrieve the user's full name
     * @return string
     */
    public function fullName()
    {
        return $this->name . ' ' . $this->last_name;
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string $value
     *
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    public function getDates()
    {
        return array( 'dob' );
    }

    /**
     * get all stats for the specified followed user with the speficied periodicity
     *
     * @param $type
     * @param $periodicity
     *
     * @return array|static[]
     */
    public static function statsWithPeriodicity($type, $periodicity)
    {
        $period_string = self::getPeriodicityModifier($periodicity);

        return DB::table('users')
                 ->select( DB::raw('strftime("'. $period_string . '", created_at) as date'), DB::raw('COUNT(*) as count'))
                 ->where('type', $type)
                 ->groupBy(DB::raw('strftime("'. $period_string . '", created_at)'))->get();
    }

    /**
     * return an SQLITE date modifier for the specified periodicity
     * @param $periodicity
     *
     * @return string
     */
    protected static function getPeriodicityModifier($periodicity)
    {
        switch($periodicity) {
            case 'daily':
                return "%Y-%m-%d";
            case 'weekly':
                return "%Y-%W";
            case 'monthly':
                return "%Y-%m";
        }
    }

    public static function getMetrics($measure){
        /*
        SELECT created_at, COUNT(*) AS count
            FROM table
            WHERE [condition]
            GROUP BY DAY(created_at);
        */
        $users = DB::table('users')
                   ->select('created_at')
                   ->get();

        return $users;
    }

}
