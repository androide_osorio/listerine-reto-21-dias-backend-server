<?php

/**
 * This model is a convenience class for managing all the user follow registrations.
 *
 * An anonymous user can follow up to 4 of the predefined stories in Reto 21 dias. This class
 * is a convenience for editing, inserting and obtaining all information of follow records
 *
 * Class FollowUser
 *
 * @author Andres Arcila
 */
class FollowUser extends Ardent {

    /**
     * Fillable Attributes
     * @var array
     */
    protected $fillable = array('following', 'user_email','created_at','updated_at');

    /**
     * specify the table this model should be attached to
     * @var string
     */
    public $table = "follow_user";

    /**
     * get all stats for the specified followed user with the speficied periodicity
     * @param $following
     * @param $periodicity
     *
     * @return array|static[]
     */
    public static function statsWithPeriodicity($following, $periodicity)
    {
        $period_string = self::getPeriodicityModifier($periodicity);

        return DB::table('follow_user')
          ->select( DB::raw('strftime("'. $period_string . '", created_at) as date'), DB::raw('COUNT(following) as count'))
          ->where('following', $following)
          ->groupBy(DB::raw('strftime("'. $period_string . '", created_at)'))->get();
    }

    /**
     * return an SQLITE date modifier for the specified periodicity
     * @param $periodicity
     *
     * @return string
     */
    protected static function getPeriodicityModifier($periodicity)
    {
        switch($periodicity) {
            case 'daily':
                return "%Y-%m-%d";
            case 'weekly':
                return "%Y-%W";
            case 'monthly':
                return "%Y-%m";
        }
    }
}