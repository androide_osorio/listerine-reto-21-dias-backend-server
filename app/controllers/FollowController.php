<?php

/**
 * Created by androide_osorio.
 * Date: 3/5/15
 * Time: 13:25
 */
class FollowController extends BaseController {

    /**
     * renders the form for following the specified story
     *
     * @param $historia
     *
     * @return \Illuminate\View\View
     */
    public function showFollow($historia)
    {
        return View::make( 'follow.form-follow' )->with( "historia", $historia );
    }

    /**
     * saves a follow request into the database
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function follow()
    {
        $follow_data = array(
            'user_email' => Input::get( 'email' ),
            'following'  => Input::get( 'historia' ),
        );

        $get_entry = FollowUser::where( 'user_email', $follow_data[ 'user_email' ] )
                               ->where( 'following', $follow_data[ "following" ] )
                               ->first();

        if ( is_null( $get_entry ) ) {
            $follow = FollowUser::create( $follow_data );
        }
        else {
            $follow = false;
        }

        if ( $follow ) {
            //Session::flash( 'message', "Gracias por seguir la historia de {$follow_data['following']} A partir de este momento conocerás el día a día de su transformación" );
            $redirectUrl = '/';

            switch ( strtolower($follow_data[ 'following' ]) ) {
                case 'virginia':
                    $redirectUrl = 'http://www.listerine.co/reto-21-dias/te-uniste-virginia-en-su-reto-de-bailar-salsa';
                    break;
                case 'alberto':
                    $redirectUrl = 'http://www.listerine.co/reto-21-dias/te-uniste-alberto-en-su-reto-de-aprender-tocar-guitarra';
                    break;
                case 'jose':
                    $redirectUrl = 'http://www.listerine.co/reto-21-dias/te-uniste-jose-en-su-reto-de-manejar-el-estres';
                    break;
                case 'cesar':
                    $redirectUrl = 'http://www.listerine.co/reto-21-dias/te-uniste-cesar-en-su-reto-de-asumir-habitos-saludables-en-su-vida';
                    break;

            }

            Event::fire( 'email.follow', array( $follow_data ) );
            return Redirect::away( $redirectUrl );
        }
        else {
            return Redirect::back()
                           ->withErrors( 'Ya estas siguiendo a ' . Str::title( $follow_data[ 'following' ] ) )
                           ->withInput();
        }
    }

    /**
     * render the unfollow confirmation page
     *
     * @return \Illuminate\View\View
     */
    public function showUnfollow()
    {
        return View::make( 'follow.form-unfollow' )
                   ->with( "email", Input::get( 'email' ) )
                   ->with( "historia", Input::get( 'historia' ) );
    }

    /**
     * perform the unfollow operation
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function unfollow()
    {
        $email = Input::get( 'email' );
        $historia = Input::get( 'historia' );

        $follow = FollowUser::where( 'user_email', $email )
                            ->where( 'following', $historia )
                            ->first();

        if(is_null($follow)) {
            return Redirect::back()
                           ->withErrors( "El correo: $email no esta suscrito a la historia de " . Str::title( $historia ) )
                           ->withInput();
        }

        if ( $follow->delete() ) {
            Session::flash( 'message', "Has dejado de seguir la historia de $historia" );

            return Redirect::back();
        }
    }
}