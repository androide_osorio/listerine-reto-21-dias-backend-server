<?php
use Carbon\Carbon;

class UsersController extends \BaseController {

    /**
     * Show the form for creating a new user
     *
     * @return Response
     */
    public function create()
    {
        return View::make( 'users.form-reto' );
    }

    /**
     * Store a newly created user in storage.
     *
     * @return Response
     */
    public function store()
    {

        $formattedDate = Input::get( 'day' ) . "/" . Input::get( 'month' ) . "/" . Input::get( 'year' );

        $user_data = array(
            'name'      => Input::get( 'name' ),
            'last_name' => Input::get( 'last_name' ),
            'dob'       => Carbon::createFromFormat( 'd/m/Y', $formattedDate ),
            'email'     => Input::get( 'email' )
        );

        $user = new Consumer;
        $user->type = 'Consumer';
        $user->fill( $user_data );

        if ( !$user->validate() ) {

            if ( $user->errors()->has( 'dob' ) ) {
                $blockingCookie = Cookie::forever( 'block_user', 'yes' );
                return Redirect::back()->withCookie( $blockingCookie );
            }

            return Redirect::back()->withErrors( $user->errors()->all() )->withInput();
        }
        else {
            //save the user and store its challenge
            $user->save();
            $challenge = $user->saveChallenge( Input::only( array( 'motivation', 'description', 'help_request' ) ) );

            if(!$challenge->exists) {
                return Redirect::back()->withErrors( $challenge->errors()->all() )->withInput();
            }

            // redirect

            Event::fire( 'email.register', array( $user_data ) );

            $redirectUrl = 'http://www.listerine.co/reto-21-dias/estas-participando-por-recibir-el-apoyo-de-listerine';

            return Redirect::away( $redirectUrl );
        }
    }
}
