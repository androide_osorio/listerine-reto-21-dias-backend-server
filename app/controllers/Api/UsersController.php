<?php namespace Api;

use \User;
use Consumer;

class UsersController extends \BaseController {

    public function index()
    {
        return Consumer::with('challenge')->get()->toArray();
    }
    /**
     * displays a user information in JSON
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $user = User::findOrFail( $id );

        return Response::json( $user->toArray() );
    }

    public function metrics(){
        $measure = Input::get('measure');


        $user=User::getMetrics($measure);

        return Response::json( $user->toArray() );
    }

    public function user_stats($type, $periodicity)
    {
        $results = User::statsWithPeriodicity($type, $periodicity);

        return $results;
    }

}
