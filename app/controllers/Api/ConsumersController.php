<?php namespace Api;

use Controller;
use \Input;
use \Response;
use \Event;
use \Auth;

/**
 * Created by androide_osorio.
 * Date: 3/3/15
 * Time: 11:22
 */
class ConsumersController extends Controller {

    /**
     * engage the user referred by the application
     * by sending an email marketing to the user
     */
    public function engage()
    {
        $userInfo       = Input::all();
        $professional   = Auth::user();
        $professionalId = $professional->id;

        Event::fire( 'email.register-pro', array( $userInfo, $professionalId ) );

        return Response::json( array(
            'message' => "{$professional->fullName()}, Gracias por recomendar a {$userInfo['name']} a aceptar reto 21 dias LISTERINE®. Ambos recibirán un correo electrónico con más información"
        ) );
    }
}