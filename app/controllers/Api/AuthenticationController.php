<?php namespace Api;

use \BaseController;
use \Auth;
use \Input;
use \Response;
use \Str;
use \App;

/**
 * This controller takes care of the token authentication
 * necessary for accessing the application's public API.
 *
 * Created by androide_osorio.
 * Date: 3/5/15
 * Time: 15:25
 */
class AuthenticationController extends BaseController {

    /**
     * gets info of the authenticated user if any
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Tappleby\AuthToken\Exceptions\NotAuthorizedException
     */
    public function index()
    {
        $user = Auth::user();
        if(!$user) {
            App::abort(401, 'You are not authorized.');
        }
        return Response::json($user);
    }

    /**
     * logs an authorized user in and generates a new
     * application token for that user, with which the
     * client can log in later to other endpoints of the API
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Tappleby\AuthToken\Exceptions\NotAuthorizedException
     */
    public function store()
    {
        $userInfo = Input::all();

        //send a bad request response if email or password are missing
        if(empty($userInfo) || !Input::has('email') || !Input::has('password')) {
            return Response::json( array(
                'message' => 'invalid username or password'
            ), 400 );
        }

        //log the user in and genrate the auth token if successful
        if ( Auth::attempt( Input::all() ) ) {
            $user = Auth::user();
            $user->token = Str::random( 32 );

            if($user->save()) {
                return Response::json( array(
                    'token' => $user->token,
                    'user' => $user->toArray()
                ) );
            }
        }
    }

    /**
     * destroys the current auth token for the specified user
     * (who must be logged in)
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy()
    {
        $user = Auth::user();
        $user->token = null;

        if ( $user->save() ) {
            return Response::json( array( 'message' => 'success' ) );
        }
        else {
            return Response::json( array(
                'message' => 'invalid token',
                'errors'  => $user->errors()->all()
            ), 403 );
        }
    }
}