<?php namespace Api;

use \FollowUser;

class FollowController extends \BaseController {

    public function index()
    {
        return FollowUser::all()->toArray();
    }
    /**
     * displays a user information in JSON
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $follow = FollowUser::findOrFail( $id );

        return Response::json( $follow->toArray() );
    }

    public function follower_stats($following, $periodicity)
    {
        $results = FollowUser::statsWithPeriodicity($following, $periodicity);

        return $results;
    }

}
