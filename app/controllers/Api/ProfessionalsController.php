<?php namespace Api;

use \Input;
use \Response;
use \Professional;
use \Carbon\Carbon;
use \Str;
use \Event;
/**
 * Created by androide_osorio.
 * Date: 3/3/15
 * Time: 11:08
 */
class ProfessionalsController extends \Controller {

    public function index()
    {
        return Professional::all()->toArray();
    }

    /**
     * stores a new professional into the database
     */
    public function store()
    {
        $info = Input::all();
        $formattedDate = Carbon::createFromFormat( 'Y/m/d', $info['dob'] );

        $info [ 'dob' ] = $formattedDate;

        $newProfessional = new Professional( $info );
        $newProfessional->type = 'Professional';

        if ( $newProfessional->save() ) {
            //Event::fire('professionals.created', array($newProfessional, Input::get('password'), Input::get('password_confirmation')));
            $newProfessional->token = Str::random( 32 );

            return Response::json( array(
                'token' => $newProfessional->token,
                'user' => $newProfessional->toArray()
            ), 201 );
        }

        $errors = $newProfessional->errors();

        return Response::json( $errors, 400 );
    }

    /**
     * displays a professional information in JSON
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $professional = Professional::findOrFail( $id );

        return Response::json( $professional->toArray() );
    }
}