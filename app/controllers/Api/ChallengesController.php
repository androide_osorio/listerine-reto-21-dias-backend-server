<?php namespace Api;

use \Challenge;

class ChallengesController extends \BaseController {

    public function index()
    {
        return Challenge::all()->toArray();
    }
    /**
     * displays a user information in JSON
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $challenge = Challenge::findOrFail( $id );

        return Response::json( $challenge->toArray() );
    }

}
