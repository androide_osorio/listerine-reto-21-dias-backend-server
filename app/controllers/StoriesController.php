<?php

class StoriesController extends \BaseController {

    /**
     * renders the correct story according to the passed url
     * @param $historia
     *
     * @return \Illuminate\View\View
     */
    public function show($historia)
    {
        $targetView = 'content.historia-' . $historia;
        $retadoresCount = FollowUser::where( 'following', ucfirst($historia) )->count();

        return View::make( $targetView )
                   ->with( 'historia', $historia )
                   ->with( 'retadoresCount', $retadoresCount );
    }
}
