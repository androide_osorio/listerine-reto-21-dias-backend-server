<?php

namespace Admin;

use FollowUser;

class DashboardController extends \BaseController {
    //las acciones aqui como siempre

    public function follower_stats($following)
    {
        $results = FollowUser::dailyStatsFor($following);

        return $results;
    }
}