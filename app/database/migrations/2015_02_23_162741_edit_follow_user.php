<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditFollowUser extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('follow_user', function($table)
		{
			$table->dropForeign('follow_user_user_id_foreign');
			$table->dropColumn('user_id');
			$table->string('user_email')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('follow_user', function($table)
		{
			$table->dropColumn('user_email');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')
				  ->onDelete('CASCADE');

		});
	}

}
