<?php

use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach( range(1, 20) as $index ) {
			User::create(array(
				'name'      => $faker->firstName,
				'last_name' => $faker->lastName,
				'dob'       => $faker->dateTimeThisCentury,
				'email'     => $faker->safeEmail,
				'type'      => $faker->randomElement(array(
					'Consumer',
					'Professional'
				))
			));
		}
	}

}