<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ChallengesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 9) as $index)
		{
			Challenge::create(array(
				'description' => $faker->text(),
				'motivation' => $faker->paragraph(),
				'help_request' => $faker->paragraph(),
				'user_id' => $index
			));
		}
	}

}