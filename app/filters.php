<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	/*header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, X-Auth-Token');

	// Pre-flight request handling, this is used for AJAX
	if($request->getRealMethod() == 'OPTIONS')
	{
		Log::debug('Pre flight request from Origin received.');
		// This is basically just for debug purposes
		return Response::json(array('Method' => $request->getRealMethod()), 200);
	}*/
});
/*
App::after(function($request, $response)
{
	$response->headers->set('Access-Control-Allow-Origin', '*');
	$response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
	$response->headers->set('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, X-Requested-With, X-Auth-Token');
	return $response;
});
*/
/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('login');
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

Route::filter('errors', function()
{
	dd('request!', Request::all());
});

Route::filter('auth.token', function() {
	$token = Request::get('auth_token');

	if(!$token) {
		$token = Request::header('X-Auth-Token');
	}

	// Fetch a user record based on api key

	$user = User::where('token', '=', $token)->first();

	if ($user) {
		Auth::onceUsingId($user->id); // Authorize the user for this one request
	}
	else {
		return Response::json(array('error' => 'Not Authorized'), 401);
	}
});