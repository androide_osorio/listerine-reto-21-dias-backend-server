<?php

/*
|--------------------------------------------------------------------------
| Event Handlers
|--------------------------------------------------------------------------
 */

/*
 * subscribe special class to handle all email marketing-related events
 * when a user subscribes, unfollows, etc.
 */
Event::subscribe(new EmailEventHandler);

Event::subscribe(new ProfessionalsEventHandler);

/*
 * Auth token event handling
 */
Event::listen( 'auth.token.valid', function ($user)
{
    //Token is valid, set the user on auth system.
    Auth::setUser( $user );
} );

Event::listen( 'auth.token.created', function ($user, $token)
{

} );

