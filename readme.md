# LISTERINE® Reto 21 Dias: Backend Server

[![Latest Stable Version](https://poser.pugx.org/laravel/framework/version.png)](https://packagist.org/packages/laravel/framework) [![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.png)](https://packagist.org/packages/laravel/framework) [![Build Status](https://travis-ci.org/laravel/framework.png)](https://travis-ci.org/laravel/framework) [![License](https://poser.pugx.org/laravel/framework/license.png)](https://packagist.org/packages/laravel/framework)

This is the repository for the LISTERINE® Reto 21 dias backend server, which centralizes all registration email sending logic.

## Requirements

* PHP 5.3.*
* Laravel Framework 4.1.*
* A web server with *cron* (UNIX) or *schtask.exe* (IIS)

## Contributors

* Andrés Felipe Osorio Patiño <ao@owak.co>
* Andrés Arcila <aa@biomd.com>

### License

Copyright 2015 Owak Digital Agency. All rights Reserved
