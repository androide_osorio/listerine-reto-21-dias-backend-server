app.factory("participanteResource", function ($resource) {
    return $resource("http://demo0007191.mockable.io/cesar",{id: '@_id'},{ 
      get: { method: "GET", isArray: false },
      query: { method: "GET", isArray: true },
  })
});

app.controller("ParticipanteController", ['$scope', '$http', '$location', 'participanteResource', '$sce', function ($scope, $http, $location, participanteResource, $sce){
	$scope.dias = participanteResource.query();

	$scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  }
}]);

app.controller("SetMessageController", ['$scope', '$http', '$location', 'participanteResource', '$sce', function ($scope, $http, $location, participanteResource, $sce){
	
		var elements = document.getElementsByTagName("INPUT");
		for (var i = 0; i < elements.length; i++) {
			elements[i].oninvalid = function (e) {
				e.target.setCustomValidity("");
				if (!e.target.validity.valid) {
					switch (e.srcElement.id) {
						case "seguireto":
						e.target.setCustomValidity("Escribe tu correo electrónico para apoyar a Cesar");
						break;
					}
				}
			};
			elements[i].oninput = function (e) {
				e.target.setCustomValidity("");
			};
		}
}]);

    