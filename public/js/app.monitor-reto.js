var app=angular.module('monitorReto', ['ngRoute','kendo.directives']);

app.controller('AppController',['$scope','$location','$http','$filter','UserFactory','ChallengeFactory','FollowFactory',function($scope,$location,$http,$filter,UserFactory,ChallengeFactory,FollowFactory) {

	UserFactory.get()
		.success(function(data) {
			$scope.users = data;

			$scope.graphicsData=data;
		});

	ChallengeFactory.get()
		.success(function(data) {
			$scope.challenges = data;
		});

	FollowFactory.get()
		.success(function(data) {
			$scope.follows = data;
		});



	// retorna que ruta esta activa actualmente
	$scope.isActive = function(route) {
		return route === $location.path();
	}

	$scope.nameOrLast = function(product){
		return product.type == 'name' || product.type == 'last_name';
	};


	$scope.createGrafInscritos = grafInscritos();
	$scope.createGrafParticipantes = grafParticipantes();

  }]);

app.config(function($routeProvider){
	$routeProvider
	.when('/',{
		templateUrl:"/reto21dias2015/public/pages/dashboard.html"
	})
	.when('/retos',{
		templateUrl:"/reto21dias2015/public/pages/retos.html",
	})
	.when('/email-marketing',{
		templateUrl:"/reto21dias2015/public/pages/email-marketing.html",
	})
	.otherwise({
		templateUrl:"/"
	});
});

app.factory('UserFactory', function($http) {

	return {
		// get all the users
		get : function() {
			return $http.get('/reto21dias2015/public/index.php/api/users');
		}
	}

});

app.factory('ChallengeFactory', function($http) {

	return {
		// get all the users
		get : function() {
			return $http.get('/reto21dias2015/public/index.php/api/challenges');
		}
	}

});

app.factory('FollowFactory', function($http) {

	return {
		// get all the users
		get : function() {
			return $http.get('/reto21dias2015/public/index.php/api/follow_user');
		}
	}

});

app.filter('split', function() {
	return function(input, splitChar, splitIndex) {
		// do some bounds checking here to ensure it has that index
		return input.split(splitChar)[splitIndex];
	}
});

function grafInscritos(){
	return {
	'theme': "Material",
	'legend': {
	  'position': "bottom"
	},
    'series': [
    {'field': 'value', 'name': 'Inscritos', 'color':'#E64A19'}
    ],
    'seriesDefaults': {
      'categoryField': "name",
      'type': 'donut',
      'area': {
      	'line': {
           'style': "smooth"
        }
      }
    },
    'tooltip': {
		'visible': true,
		'template': "#= series.name #: #= value #"
	}
  }
}

function grafParticipantes(){
	return {
	'theme': "Material",
	'legend': {
	  'position': "bottom"
	},
    'series': [
    {'field': 'value_alberto', 'name': 'Follows Alberto'},
		{'field': 'value_virginia', 'name': 'Follows Virginia'},
		{'field': 'value_cesar', 'name': 'Follows Cesar'},
		{'field': 'value_jose', 'name': 'Follows José'}

    ],
    'seriesDefaults': {
      'categoryField': "name",
      'type': 'line',
      'area': {
      	'line': {
           'style': "smooth"
        }
      }
    },
    'tooltip': {
		'visible': true,
		'template': "#= series.name #: #= value #"
	}
  }
}