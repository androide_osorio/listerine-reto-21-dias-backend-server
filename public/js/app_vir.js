app.factory("participanteResource", function ($resource) {
	return $resource("http://demo0007191.mockable.io/virginia",{id: '@_id'},{ 
		get: { method: "GET", isArray: false },
		query: { method: "GET", isArray: true },
	})
});

app.controller("ParticipanteController", ['$scope', '$http', '$location', 'participanteResource', '$sce', function ($scope, $http, $location, participanteResource, $sce){
	$scope.dias = participanteResource.query();

	$scope.trustSrc = function(src) {
		return $sce.trustAsResourceUrl(src);
	}
}]);




