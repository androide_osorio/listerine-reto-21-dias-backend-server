var app = angular.module('myApp', ['ngResource', 'ngStorage']);

app.factory("participanteResource", function ($resource) {
    return $resource("http://demo0007191.mockable.io/participantes",{id: '@_id'},{ 
      get: { method: "GET", isArray: false },
      query: { method: "GET", isArray: true },
  })
});

app.controller("ParticipanteController", ['$scope', '$http', '$location', 'participanteResource', function ($scope, $http, $location, participanteResource){
	$scope.dias = participanteResource.query();
	console.log($scope.dias);
}]);


 app.controller('Ctrl', ['$scope', '$localStorage', '$sessionStorage', function($scope, $localStorage, $sessionStorage) {
    $scope.$storage = $localStorage.$default({
    counter: 5
});


}]);

