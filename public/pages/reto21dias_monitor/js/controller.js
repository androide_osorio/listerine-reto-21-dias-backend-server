var app=angular.module('monitorReto', ['ngRoute','kendo.directives']);

app.controller('AppController',['$scope','$location',function($scope,$location) {

	$scope.users=[
		{id:1,name:"prueba",last_name:"uno",dob:"12/12/1990",email:"aa@biomd.com",},
		{id:1,name:"prueba",last_name:"uno",dob:"12/12/1990",email:"aa@biomd.com",},
		{id:1,name:"prueba",last_name:"uno",dob:"12/12/1990",email:"aa@biomd.com",},
		{id:1,name:"prueba",last_name:"uno",dob:"12/12/1990",email:"aa@biomd.com",}
	];

	$scope.isActive = function(route) {
		return route === $location.path();
	}

	$scope.graphicsData ={
		transport: {
			read: {
				url: "http://demo4648965.mockable.io/grafData",
				dataType: "json"
			}
		}
	};


	$scope.createGrafInscritos = grafInscritos();
	$scope.createGrafAlberto = grafParticipante("alberto");
	$scope.createGrafVirginia = grafParticipante("virginia");
	$scope.createGrafCesar = grafParticipante("cesar");
	$scope.createGrafJose = grafParticipante("jose");

  }]);

app.config(function($routeProvider){
	$routeProvider
	.when('/',{
		templateUrl:"pages/dashboard.html"
	})
	.when('/retos',{
		templateUrl:"pages/retos.html",
	})
	.when('/email-marketing',{
		templateUrl:"pages/email-marketing.html",
	})
	.otherwise({
		templateUrl:"/"
	});
});

function grafInscritos(){
	return {
	'theme': "Material",
	'legend': {
	  'position': "bottom"
	},
    'series': [
    {'field': 'comments', 'name': 'Inscritos', 'color':'#E64A19'}
    ],
    'seriesDefaults': {
      'categoryField': "month",
      'type': 'pie',
      'area': {
      	'line': {
           'style': "smooth"
        }
      }
    },
    'tooltip': {
		'visible': true,
		'template': "#= series.name #: #= value #"
	}
  }
}

function grafParticipante(participante){
	return {
	'theme': "Material",
	'legend': {
	  'position': "bottom"
	},
    'series': [
    {'field': 'comments', 'name': 'Follows', 'color':'#E64A19'}
    ],
    'seriesDefaults': {
      'categoryField': "month",
      'type': 'pie',
      'area': {
      	'line': {
           'style': "smooth"
        }
      }
    },
    'tooltip': {
		'visible': true,
		'template': "#= series.name #: #= value #"
	}
  }
}